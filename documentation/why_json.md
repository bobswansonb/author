# Why JSON in AUTHOR?

(updated 4/30/2023)

The configuration and metadata
input files to AUTHOR are written
in Javascript Object Notation (JSON).

## Why?

I have worked with various input file
formats over the years, and this format
seems to be the most compatible with
software, while being fairly easy
to be human-modified.

For instance, XML is a major data standard,
but is very difficult for a human to
read and modify.

I tried YAML, but that notation is hyper-sensitive
to indentation. The slightest error, and
while the Java code still runs, it throws
errors that make little sense and are
difficult to run down.

## How?

Nearly all of the JSON files used by AUTHOR
consist of simple lists of strings. Some are pairs
of keyword and value, still they are all strings.

Each grouping of such data is named, which
makes it easy to find, while editing the
metadata.

Example:


```

"project":[

{

"PROJECT_AUTHOR":"Dr. Steven J. Berlin",

"PROJECT_PUBLISHER":"Military Postal History Society",

"PROJECT_COPYRIGHT":"Dr. Steven J. Berlin, 2021-2023. However, this information can be used freely for education and research.",

"PROJECT_TYPE":" presentation "
	}


 ]
```
 
This data grouping is named "project", and lives in the
"project.json" file. It contains keyword/value
pairs for important metadata. The keyword is on the left side of the colon (:)
character, while the value is to the right of the colon. These pairs end
with a comma, except for the last line.

This "metadata"  can easily be modified by
the AUTHOR user to reflect the critical information for the project
being created. (It is best to use a simple text
editor to modify JSON. It is NOT recommended that you
use a word-processor, such as Word.

Note that each string (keyword and value) is surrounded
by double quotes. Each line is terminated by a comma
(except the last string set). The set of strings
is surrounded by curly braces  ({}).

Since the strings are surrounded by quotes, you have to
"escape" any quotes that appear as text in your
data. Example:

```
"PROJECT_PUBLISHER":"Military Postal History \"Society\"",
```

Note that, to include the double quotes surrounding
the word "Society", you have to place a backslash before
the desired double quote character. This type of escaping
MUST balance! The errors issued by the JSON processor
can be confusing, so use with caution!


You can use the example JSON files provided with
this project, to enter your project metadata.

- - - - - -

The "canned" strings used to create the
output of AUTHOR are, for the most part,
embedded in the "Creat*.json" files, one
for each output format. By modifying these
files, you can change the look and feel of
the appropriate e-document.

Example:

```
"cover_page":[

"<!-- inside the first flow of the document --><fo:block text-align=\"center\">\n<!-- cover image follows -->\n",

"<fo:external-graphic src=\"pics/${PROJECT_COVER_IMAGE}\"  content-width=\"scale-to-fit\" content-height=\"80%\" />",

"</fo:block>\n"
],
```

This fragment is used by the Java code to create part of
an Apache FOP file (which is later made into PDF).
This is true XML, and should only be changed by
someone familiar with XML and Apache FOP. (Note the
use of escaping the double quotes, as discussed
above.)

Note the marker item embedded in the value string. 
It is a name (in this case ALL CAPS) surrounded by
curly braces ({}), with a dollar sign ($) at the front.

Fields such as this are noted by the Java code,
and the code replaces the inline value with the value of the same name 
encountered in the metadata (project.json) 
file. Note that mis-spellings of the field names
can cause troubling bugs, so do be careful.

This feature
allows the "boilerplate" nuts and bolts code
to remain the same, while preventing the user from
having to change complex XML files. The more-easily
entered data in the "project.json" file is moved
into the output, only when it is called for.

Similar JSON named entities are used for boilerplate
information, such as HTML header text, etc.

Even these "boilerplate" values can be altered by
the savvy AUTHOR user, when issues of style or
layout need changing.

## History

There was an attempt to "parameterize" all of
the layout elements, such as HTML tags, etc.
It turns out that many complex HTML entries
cannot easily be entered from "canned" input.
Sadly, that means that certain HTML output is
created programmatically inside the Java objects, while other
output is fully parameterized in the JSON files.
