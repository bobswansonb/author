# Advantages and Disadvantages of Using AUTHOR

Use of the AUTHOR system comes with both advantages and disadvantages.
Carefully study this information, before jumping into a new project.

## ADVANTAGES


- Simple input, using plain text, regardless of output format (a text-heavy document can be created by simply typing in the content)

- Wide variety of e-doc output formats created, using well-established
   free tools (Adobe FOP, Calibre)

- Limited number of markups (some projects may not need much markup at all)

- Can handle large numbers of images (most "publish" systems choke
   on large numbers of images)
   
- Easy to add hyperlinks to an e-document, so creating a fully
   interactive user reading experience
   
- While not the official tool of choice, does a pretty good job of
   creating REVEAL.js web-based presentations.
   
- NOT a WYSIWYG editor

- Simple layout

- Very fast. Can create a 1,000 page document with 2,000 images in a few seconds. This allows quicker turnaround of proofs and versions of the document.

- Output doesn't look like a fully-designed, sophisticated layout, such
   that created by Microsoft Publisher (yes, that is an
   advantage to some readers)
   
- Output is designed to work well with small screens (phones), print output,
   and flow-specific reading tools, such as EPUB and
   Kindle
   
- Print output (by way of FOP-PDF) is very customizable for fonts, page size, column counts, margins, etc. 


## DISADVANTAGES
- - - - - - - -


- Simple layout (yes, some people want 5 columns 
   across, with span across columns 2 and 3 only in certain sections of the document. sorry.)

- Does not support many sophisticated markups

- NOT a WYSIWYG editor

- Under "simple layout", note that this product does NOT flow
  text around images
  
- Output doesn't look like a fully-designed, sophisticated layout, such
   that created by Microsoft Publisher
   
- The Technical Editor who actually executes AUTHOR and produces the e-documents, must have a decent knowledge of file management and execution of programs on a computer. They also need to be able to manipulate images, if the e-document uses them. They need to be able to carefully edit JSON in a text editor, and in some cases, they may even have to program in Java to achieve sophisticated layout results.
  
  
