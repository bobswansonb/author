# About the AUTHOR Documentation 

The "documentation" folder will contain various "how-to" writeups. Examples will include:

1. The AUTHOR language (what there is of it) [This will probably be written in markdown.]

2. Javadoc for book creation code (maybe in the future)

3. "how-to" create e-documents from AUTHOR input, many of which will point
    to the "examples" folder

4. Use of JSON for metadata and "boilerplate" in AUTHOR. And why JSON? 

5. Suggestion for the format of images used with an AUTHOR e-document, especially
for documents with large number of images. 

6. Enhanced use of A: (anchor) tags for destination groupings.
   
7. Document about "roles" and "flows" for using AUTHOR. Each
   role is described, and the interchange between the two
   roles is explained. The two roles are: (1) Content Creator
   and (2) Technical Editor. 

8. Document describing the advantages and disadvantages of
   using AUTHOR for an e-document project. 

9. Warning "Don't Use AUTHOR". A little tongue-in-cheek, but the
fact is that this product won't work for everyone, and
may be a waste of time for certain projects.

