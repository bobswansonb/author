# Flow of Data Within the AUTHOR System

(updated 4/30/2023)

This is a short writeup on how data flows through the AUTHOR system. A small amount of development history is included.

## Introduction

Originally, this software system was created to make two output formats: HTML and PDF. The PDF was created directly using
the Adobe PDFBox software package. This is great software, and I use it all the time for PDF manipulation. However,
direct PDF creation and layout is just too difficult. I changed over to Apache FOP instead.

As a result, the release version of the AUTHOR system reads text and creates text.

## PDF Output

For PDF output, the system reads input and creates ".fo" files. These files must be combined
and become input to the Apache FOP system. That system can easily create excellent PDF output. 
It does great layout, and is very, very flexible. Note that images must be available
to the FOP program, when it creates the PDF.

PDF output can come in two flavors: interactive and print.

## PDF Print

For PDF Print, the default layout is 2-column, 8 1/2 x 11 inch. It is pretty easy to manipulate the FOP
data to create other formats. (That is, the FOP input data can be manually retained as an
"intermediate" format, and sent through the FOP program repeatedly.) 

In print format, there are page numbers, and all cross-references
("see also") are page numbers. All indexes refer to page numbers.

## PDF Interactive

For PDF Interactive, the default layout is 1-column 8 1/2 x 11 inch. Again, it is pretty easy to 
manipulate the FOP data to create other formats. 

In Interactive format, there are no referrable
page numbers, and all cross-references are clickable links. Indexes are interactive, and stored 
as "bookmarks/contents". Most PDF readers support clickable links, and offer display bookmarks/contents for ease 
of navigation.

## PDF Requirements

For PDF processing, you must install Apache FOP on your computer. I will provide example scripts
that invoke it.

## HTML Output

For HTML output, your generally create a single HTML file, which contains
the the entire document. You may, instead, create one HTML file for each
SECTION:). 

As designed, the
layout is dependent on use of certain CSS and Javascript (included). 
Over the years, I have settled on the Vanilla layout system,
which works very well on all machines (phones, tablets, computer screens),
and is carefully maintained by the folks who bring us Ubuntu.

Right now, there are two "flavors" of view, "dark", and "light".
The color of the HTML page background 
is the primary difference between these. Some people who view
the final documents in their web viewers, prefer a darker
background for image-heavy products.

Code remains in the product for "Skeleton", and an even
older system called "POEM".

It is not difficult to alter
the JSON configuration files to use another HTML layout scheme. The reason for using
these particular layout systems is that they work well on a wide variety of devices, from
computer screens, to tablets, to phones, with a minimum of code. I understand that there
are increasingly prevalent layout systems for web-based document viewing, and look forward
to a time when the output of AUTHOR can create such documents.

# Website-In-A-Box

The HTML output, along with images, CSS and Javascript, can be packaged into a ZIP file for 
distribution as a "website-in-a-box" format. Such a package can be read by any user on any device
that supports a decent web browser. This method of document distribution is nice because
it does not tie the end reader to any particular viewing software (Kindle, Nook, PDF Reader, etc).

# Kindle Output

We used to try to meet the ever-changing demands of Amazon for the Kindle formats.
Now, we use the Calibre program to perform our conversions. This is an outstanding
product, and is free.

We create a single-page HTML version of our document using AUTHOR. We import
the HTML and images into Calibre. From there, we can perform a
conversion to AZW3, a currently-accepted Kindle format. There are no
embedded restrictions on the content in the straight AZW3 format,
but you should be able to upload that file into Amazon, in order
to sell it through them. Amazon will add protection of the file.

# EPUB Output

Follow the flow for the Kindle output, as described above. Inside
Calibre, you just specify EPUB output, and you will then have
this type of e-book format.

(At one time, I tried to directly create EPUB files. This proved to be
very frustrating, and nothing I tried creating could pass the EPUB syntax tester.)

# REVEAL Output

For "reveal.js" web-based presentations, the program directly creates a single
HTML file. That file expects the presence of the appropriate "reveal.js" support
files. The assumption is that such a presentation is landscape-oriented, and looks
and behaves as if it were a PowerPoint (tm) presentation. The "reveal.js" system
has proved to be excellent, working well on a variety of computer screens, tablets,
and phones.

# About Calibre

Note that once you have imported your HTML file (as created
by AUTHOR) into Calibre, it can be edited in place within
Calibre. This is a very powerful feature of the excellent
tool. However, NOTE that, once you have made editorial
changes to your document inside of Calibre, those changes
will not be "sent back" to AUTHOR, or your original AUTHOR
input file. Hopefully, the only changes you will want to
do with Calibre are cosmetic.
