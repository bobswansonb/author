# Image Tags in AUTHOR

(updated 5/1/2023)


One of the primary design points in AUTHOR, is the attempt to make it easy to insert an image into a text. If your e-document is to be just a set of words, you may not need all of the extra features of AUTHOR, including images.

There are 3 image tags, their format largely based on historical use. Within each are several variants, so it is best to pick one and stay with it, rather than mixing image tags.

```

IM: (modern usage, local image)

IM2: (ancient usage, probably don’t do this)

RIM: (modern usage, remote image, see caveats)

```

There are several types of implementations. A lot of  kluging has taken place, so it is rather complex. Best for the user to stick with ONE format, probably preferring to use the SIMPLEST.

Note that all of the “newer” formats REQUIRE an “anchor” name, which will be used to allow referral to the image as a separate entity (for instance, in See Also). These anchor names **MUST BE UNIQUE**, and cannot collide with SECTION: names!

Also, the “newer” formats **ASSUME** that the next line of AUTHOR input is the caption. You can specify an empty caption using the “EMPTY:” tag on the next input line.

## Tag with 2 entries:

2 entry tags  work for shortened LOCAL (non-remote image, is included in the final e-book):

1) main image filename WHICH IS THE SAME AS the thumb image filename. Each image type is located in its own storage area, so “xyz.jpg” can be both a thumbnail filename (living in a different directory), and main image filename (living in the main image directory)
2) anchor name (unique)

Next Line: caption for above

(images appear as stored, no size override during display)

## Tag with 3 entries:


3 entry tags work for shortened **LOCAL** (non-remote image, is included in the final e-book):
 
1) main image filename
2) thumb image filename (if these names are THE SAME, then it might be best to use the 2-entry type)
3) anchor name (unique)

Next input line is the caption.

(images appear as stored, no size override during display)
     
   
3 entry tags work for **REMOTE** (the image must have an accessible URL. Thumbnails are not used.) The REMOTE format will not work with e-books, such as EPUB or Kindle, since those are supposed to be self-contained!):

1) main image url (uses main filename)
2) alt text (would otherwise be the thumb field)
3) anchor name (unique)

Next input line is the caption

(images appear as stored, no size override during display)

## Tag with 6 entries:

(Yes, 6 entries...) The 6 entry tags are the ORIGINAL format (created for the facilities book project). This arrangement is probably much too complex for ordinary use (and some fields are NO LONGER used) these images are ALWAYS LOCAL:

1) main image filename
2) thumbnail image filename
3) pdfscale
4) htmlwidth (optional)
5) pdfuse
6) anchor name (unique)

Next input line is the caption.
         
If “htmlwidth” is specified, it is a pixel count. The only Sink’s that use this value are the REVEAL and HTML sinks. This width is not used by any other output, such as PDF, EPUB, Kindle, etc. If this width is not specified, the image appears in its given size.

What we are saying regarding the “scale” or “width” of images, is that the author of the e-document should create the images with the desired size. Note that such creation includes ensuring that the files sent to PDF, for instance, have the correct DPI setting (72 or possibly 144), particularly for print-on-demand. Large images are squeezed into the PDF layout by the FOP tool, so if you have a very large image (e.g., 3,000 pixels wide) it will make the PDF file large, but will not appear in the document page any differently from an 800 pixel wide image. Only when the user zooms in with their PDF viewer, will they see the detail in the larger image. Some print-on-demand print devices choke on large images, or images that have incorrect DPI values.

Not all of their fields are currently used. For instance “pdfscale”, and “pdfuse” are most probably ignored, which is why we went with a much simpler format.
         *
NOTE
FOR ALL THESE TYPES, the CAPTION is considered to be the next input line. If you want an empty caption, you must use “EMPTY:”



## IM2: old format

Kept for compatibility with the very first use of AUTHOR, which was my main military history book. It had much more complex image detailing. Subsequent experience using AUTHOR for other projects, showed that images can be dealt with much more simply. Otherwise, you can be into the weeds rapidly. This type of image is assumed to be LOCAL.

1) thumbnail image filename
2) main image filename
3) pdfscale, was tried and didn’t work well, so this field is ignored
4) htmlwidth (optional, see above. Only used by HTML-related Sink’s, such as REVEAL and HTML)
5) pdfuse, now ignored
6) image CAPTION – this is the primary reason for this format, where the caption is embedded in the IM2: tag, NOT on the next input line.

However (!) such a setup does not allow for multi-line captions. I tried using the double colon (::) for the caption lines, and it probably works. However, this is old stuff, best not used.

7) OPTIONAL anchor name. This is the only image descriptor format that allows you to insert images that have no identification (and thus, cannot be referenced in the e-document. Indeed, there won’t be an anchor, if you leave it off. Good idea? Don’t know.

## Example Image Tag Usages:

```
IM:myimage.jpg::image1
caption here
```

Both primary and thumb are the same name and are present in local storage.

Anchor name is “image1” and can be referenced by a S: (see-also).

```
IM:myimage2.jpg::mythumb2.jpg::image2
caption here
```

Primary image is “myimage2.jpg” and thumb is “mythumb2.jpg”. They just both be present in local storage.

Anchor name is “image2” and can be referenced by a S: (see-also).

(Anchor names MUST BE UNIQUE)

```
RIM:http://mywebsite.com/pics/myimage3.jpg::My image number 3::image3
```

The image used in HTML (and REVEAL) e-documents will be loaded from “mywebsite.com”. This format cannot be used for EPUB or Kindle, because they must be self-contained.

Second field is the “alt=” text for the HTML tag. Hovering over the image in most browsers will show this text, but if the image is missing, this text should be substituted.

Anchor name is “image3” and can be referenced by a S: (see-also).

(Anchor names MUST BE UNIQUE)

Examples are not shown for older image formats. Don’t use them.

