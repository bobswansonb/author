# Using AUTHOR: Special Anchor Tag Values

#### (updated 4/30/2023)

## Introduction

When the need appeared to form lists of "links", it was discovered that
the "A:" anchor tag could be used quite effectively.

## Typical Anchor Use

For example, if the AUTHOR input contains:

\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\--

A:comehere

Here is some text that I want to link to later.

...

...

When you want to

S:comehere::jump to the desired text, click here.

\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\--

The example above shows the simple use of the anchor tag.

## 

## Group Anchor Use

Now, what if you have a number of anchors scattered throughout your
document, and you want to show a single list of links that will allow
jumping to any of them?

A:name::group::narrative

This anchor markup does double duty. It not only provides a destination
for a jump, but it also allows anchors to be "grouped". The "group"
name in the A: markup is used to allow you to group locations by some
scheme of your own creation. The "narrative" on the end, is the
wording used for the link created.

Example:

\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\--

(content)

A:choc:ICECREAM::Chocolate Ice Cream

(content)

A:van:ICECREAM::Vanilla Ice Cream

(content)

A:straw::ICECREAM::Strawberry Ice Cream

(content)

Summary

H2:Various ice cream products in this document are:

MAKELIST:ICECREAM

(content)

\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\--

The "MAKELIST:" markup will take all of the assembled anchors that
have that name (ICECREAM) and create a list in place that has all of the
links to those locations. The "narrative" text will be the text for
the jumps.

Note that any single anchor that has been grouped, can still be referred
to by the "see also" (S:) tag.
