|     |     |     |     |     |     |
| --- | --- | --- | --- | --- | --- |
| **Tags Used by AUTHOR System** |     |     |     |     |     |
|     |     |     |     |     |     |
| **Updated 5/1/2023** |     |     |     |     |     |
|     |     |     |     |     |     |
|     |     |     |     |     |     |
| **Tag** | **Cont1** | **Cont2** | **Cont3** | **Cont4** | **Notes** |
|     |     |     |     |     |     |
| No tag |     |     |     |     | Any text between empty lines is considered to be a paragraph  <br>(there are a few exceptions, see LIST:) |
|     |     |     |     |     |     |
| P:  | text  <br>(optional) |     |     |     | Start paragraph, ends at next P: or empty line. Note that  <br>the P: can have text content, if you wish. Paragraphs can terminate  <br>due to many other markups, such as images, SECTION:s, etc. |
|     |     |     |     |     |     |
| PROJECT: |     |     |     |     | required first line, identifies project |
|     |     |     |     |     |     |
| INDEXLOC: | URL |     |     |     | PROPOSED as a needed reference to be used in AUTHOR  <br>file to link to the Index location (only important with HTML  <br>Output) |
|     |     |     |     |     |     |
|     |     |     |     |     |     |
| L:  | URL | Narrative |     |     | URL Link (usually to outside world), narrative is text link |
|     |     |     |     |     |     |
| A:  | Anchor Name | Group Name  <br>(opt) | Link  <br>Narrative  <br>(opt) |     | Anchor specifies a location to be linked to internally  <br>Names must be unique. 2 additional options specify that this  <br>Anchor belongs to a “group” which can be dumped upon  <br>User request. Narrative text belongs to the dump output  <br>See separate writeup |
|     |     |     |     |     |     |
| MAKELIST: | group name |     |     |     | Gathers content that refers to all of the anchors (A:)  <br>That were specified with the same group name. Created as a bulleted  <br>List with links (and anchor text) to each anchor location.  <br>For named anchors only (not image anchor names) |
|     |     |     |     |     |     |
| PRE: | none |     |     |     | Starts dump of pre-formatted text. Especially newlines  <br>Are honored. Group of lines terminate with ENDPRE:  <br>No special formatting occurs! (see BPRE: and IPRE:) |
|     |     |     |     |     |     |
| ENDPRE: | none |     |     |     | Terminates a pre-formatted group of text lines |
|     |     |     |     |     |     |
| BPRE: | none |     |     |     | Starts dump of pre-formatted text. Especially newlines  <br>Are honored. Group of lines terminate with ENDPRE:  <br>Text is **boldface** (see also PRE: and IPRE:) |
| IPRE: | none |     |     |     | Starts dump of pre-formatted text. Especially newlines  <br>Are honored. Group of lines terminate with ENDPRE:  <br>Text is **italic** (see also PRE: and BPRE:) |
|     |     |     |     |     |     |
| S:  | Anchor Name | Text | TOP  <br>(optional) |     | SEE ALSO link. The destination anchor name must  <br>Exist. For internal document use only. (See L:) Note that anchors  <br>May not be in exactly the place wanted as they are line position  <br>Oriented. Note also That images (IM:) are required to have  <br>anchors and thus Can be S: destinations without other markup, use  <br>Of the TOP (3rd position) always jumps to the top of  <br>The page (HTML only) PREVIOUS line should end with space character |
|     |     |     |     |     |     |
| H1: H2:  <br>H3: | Heading  <br>Text |     |     |     | Heading, levels are typical of HTML. Cannot be inside a bulleted  <br>List, text content is ONLY this heading, next line in AUTH file is  <br>Treated normally |
|     |     |     |     |     |     |
| I:  | text |     |     |     | Text will be **italicized**. Previous line should **end with one or more space**  <br>Character |
|     |     |     |     |     |     |
| B:  | text |     |     |     | Text will be **boldface**. Previous line should **end with one or more space**  <br>Character |
|     |     |     |     |     |     |
| IN: | entries,  <br>Separated  <br>By : |     |     |     | One of more index entries, separated by colon (:)  <br>Will show up in “general” index. Location is close to  <br>Position of IN: line, but really can’t be the top of a  <br>SECTION:. Note! No empty index entries are allowed.  <br>So (“”) will cause an error during parsing. |
|     |     |     |     |     |     |
| SECTION: | Section  <br>Name | Long title | Auxiliary  <br>Information  <br>(optional) |     | Main grouping: think “chapter”, “appendix”, etc. Title is put  <br>In as heading for this section. Section name must be a legal  <br>Filename for your computer system, and particularly when  <br>Output is HTML, it must be addressable in a browser.  <br>For other formats, the internal links must also be  <br>Created using simple letter, digit, underline. So, keep the  <br>Section name simple, please.  <br>  <br>For HTML output, auxiliary information can  <br>Be added to the HTML file as metadata, see writeup. |
|     |     |     |     |     |     |
|     |     |     |     |     |     |
| LIST: | font size  <br>(optional) |     |     |     | start a bulleted list, ends with LISTEND:. If number follows  <br>LIST: it is PERCENT size of the font used in the  <br>List elements. For instance, smaller list items could  <br>Be created by LIST:90 (90%) Rules: certain tags  <br>Cannot be inside a LIST:, such as images and headings,  <br>No P:, A:, IN:. No blank lines inside LIST:  <br>Formatting, such as B: I: should be OK. See separate writeup |
|     |     |     |     |     |     |
| LISTEND: | none |     |     |     | ends a bulleted list, started with LIST: |
|     |     |     |     |     |     |
| LI: |     |     |     |     | Item in a bulleted list, it is a sub-paragraph that ends  <br>With the next LI: item or end of LIST: |
|     |     |     |     |     |     |
| TABLE: |     |     |     |     | starts a very simple table. This structure is available  <br>In all formats created by AUTHOR, but nothing fancy in  <br>Formatting. See separate writeup |
|     |     |     |     |     |     |
| RCELLS: | cell1 | cell2 | etc |     | simplified way to enter contents of all cells for a Table row.  <br>Each cell value is between (::) markers |
|     |     |     |     |     |     |
| ROW: |     |     |     |     | slow way to enter a table row, multiple CELL: contents follow, up  <br>To ENDROW: |
|     |     |     |     |     |     |
| CELL: | cell  <br>Contents |     |     |     | a cell value within the encompassing ROW: |
|     |     |     |     |     |     |
| ENDROW: |     |     |     |     | end of row, the slow way to enter table contents |
|     |     |     |     |     |     |
| ENDTABLE: |     |     |     |     | terminates simple table |
|     |     |     |     |     |     |
| IM: |     |     |     |     | image (new format), for all images, see separate writeup |
|     |     |     |     |     |     |
| RIM: |     |     |     |     | remote image (see separate writeup) |
|     |     |     |     |     |     |
| IM2: |     |     |     |     | image (old format, see writeup) |
|     |     |     |     |     |     |
| E:  |     |     |     |     | E: or EMPTY: is a forced empty line, used as marker (see images) |
| EMPTY: |     |     |     |     |     |
|     |     |     |     |     |     |
| **Old Entries, no longer used** |     |     |     |     |     |
|     |     |     |     |     |     |
| STATE: |     |     |     |     | formed a special “section” for each state of the U.S. |
|     |     |     |     |     |     |
| ABBREVIATIONS: |     |     |     |     | special appendix with special formatting |
|     |     |     |     |     |     |
| CITY: |     |     |     |     | formed a special “section” for each city within a state of the U.S. |
|     |     |     |     |     |     |
| ENDCITY: |     |     |     |     | terminated special section for a city |
|     |     |     |     |     |     |
| FAC: |     |     |     |     | a single listing for a facility within a city within a state |
|     |     |     |     |     |     |
| ENDFAC: |     |     |     |     | ended a facility listing |
|     |     |     |     |     |     |
| ABB: |     |     |     |     | an abbreviation entry within the ABBREVIATIONS: section |
