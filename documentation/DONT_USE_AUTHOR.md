# A SIMPLE ALTERNATIVE TO AUTHOR

Frankly, the AUTHOR system I've devised is not for every 
content creator, nor for every type of document. 

As noted before, one of the most
powerful features is handling large numbers
of pages, links, and images. The syntax of AUTHOR
makes it easy to insert hyperlinks, so making
an e-document fully interactive.

If you don't need a system with these features,
and don't want something this complex, I
recommend the following approach:

1. Create your document using LibreOffice. If you
   are only interested in the text, this is
   a very good (and free) product to use.
   
2. Install "Calibre" on your computer (also free).

3. Import your completed LibreOffice Writer file (ODT) into
   Calibre
   
4. VERY CAREFULLY update the metadata
   information for your product, using
   Calibre. Select or create a good cover image,
   and add to the metadata.
   
5. Create your e-document, using the
   "convert books" feature of Calibre. I recommend creating both EPUB and AZW3 (Kindle) output formats.
   
6. Carefully review your created e-documents
   in Calibre, using its stand-alone e-viewers.

7. If you need to "tweak" the document, you can use the very powerful editor in Calibre. However, once you make changes to the document in Calibre, those changes stay there, and you should continue to use Calibre for all further modifications of the document.

8. Publish your e-documents
