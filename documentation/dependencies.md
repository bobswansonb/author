# Dependencies for the AUTHOR System

The AUTHOR system is primarily a text-to-text program. It reads the
AUTHOR text, and creates text files that are used by other programs
to make final e-document output. Exceptions to this statement include the HTML, HTML(single page) and REVEAL creators, which makes the output file(s) ready to use.

Thus, this product depends on other systems to do a lot of
the heavy lifting (THANKS!).



## Required Java JAR's (for Java compilation and execution)

- - - - - - - - - - - -

Since JSON is handled by the Java code, you need the BFO JAR. I've included this in the "lib" area. [API](https://faceless2.github.io/json/docs/com/bfo/json/package-summary.html)


## Required CSS Files (for use when output is HTML)

- - - - - - - - - - - - - -

vanilla-framework-version-2.0.1.min.css, at a minimum. Actually, so far
using later versions, including 3.0, has worked fine (Using features 
of Vanilla for both light (default) and dark web pages.)

[Vanilla project](https://vanillaframework.io/)

Previous, but have been stubbed out:

 - skeleton.css (for one mode of HTML, not the default)

 - poem.css (was default mode of HTML, no longer)



## Required Javascript Files (for use when output is HTML)
- - - - - - - - - - -

 - utility_v.js (for Vanilla HTML output, to handle images)


 - reveal.js (for REVEAL mini-presentations ONLY. NOTE that several other components of REVEAL are needed) [reveal.js](http://lab.hakim.se/reveal-js/#/)



## Required Helper Apps

- - - - - - - - - -

   
 - Apache FOP -- used only if you want to make PDF files. Get
   the command-line version from the Apache website. See
   info on "Fonts" below. [Apache FOP](https://xmlgraphics.apache.org/fop/)
   
 - Calibre -- used to make EPUB and AZW3 formats from a single page HTML output of AUTHOR. Also very
   useful for conversion of e-document formats. It has its own
   viewers, which are pretty nice. In addition the book
   "editor" within Calibre is great when small tweaks
   are needed. Note, however, that once you change
   the e-book within Calibre, it stays there, and can only
   be exported to the formats that Calibre support.
   Get from [website](https://calibre-ebook.com/)

 - EPUB viewer(s) There are various ones available on different computing
 systems. Aside from Calibre's built-in viewers, you can use many of
 the viewer products out there to check your work.
 
  - A web browser (I try to use many, to make sure that the layout is right)
   
   
## Required Fonts

- - - - - - - - 

Fonts can become a rather sticky issue. Your computer may have
different fonts from those used by the person viewing your
e-document. In most cases, this is not a big issue. AUTHOR
keeps the font usage quite simple. We are not a word processor!

The biggest issue I've run into is embedding fonts for
published PDF files for print-on-demand. For instance, to
upload a PDF to LuLu (lulu.com), you must have embedded all
fonts in the PDF file. Be sure you have them around, and make sure
they are specified. This is a FOP issue, and must be addressed
in this case.

For interactive PDF, or PDF that will be read on a computer only,
no big deal, each machine will find an appropriate font, and use
it. Kindle is not an issue at all, nor is EPUB output. The
HTML output is not font-specific. You could alter the CSS
to make it such, it's your call.

For the examples I provide, I've uploaded, and use, the Arial and Courier 
fonts. You may use any fonts you want. In the case of printed PDF (especially for 
Print on Demand -- POD), you must embed those fonts. The FOP configuration file 
must specify embedded fonts, and their locations. Non-embedded fonts can be specified
in the FOP input, and the running software will pick something "close". This
includes that possibility that the PDF reader being used will also
pick something "close".

Note that the Arial and Courier fonts are FOR EXAMPLE ONLY, I'm not a font
distributor or seller.

