# Image Configurations and AUTHOR
#### ***(updated 4/30/2023)***

One of the positive features of the AUTHOR system is the ability to create an e-document with large numbers of images.

For instance, my WW I book is about 630 printed pages, and contains nearly 1,000 images. There are few "publishing" programs that can handle that many images.

There is a strategy associated with using images with AUTHOR, partly caused by the limitations of the final e-document formats.

First of all, note that most of the e-document creators work best with JPG images. Any problems or limitations associated with other image formats, depend entirely on the processor (Adobe FOP, Calibre, browsers, etc).

One of the most important issues is file sizes. Think of the storage and download time needed for an e-document that contains 1,000 high-quality images....

## **Suggested Approach**

What I have done is the following:

All images used in the e-document are resized to 500 pixels width. These images are stored with a DPI setting of 144 pixels per inch.

This may seem a bit harsh, given the desire of the final audience (or the author) to want high-quality images in the book. However, you should consider the final size of a PDF file created with this many images. Also note that if you publish with Amazon Kindle, the price of your e-book depends on the size of the download.

The reason for the 144 DPI setting is the handling of PDF output by Adobe FOP. If the DPI setting is too low, the image placed in the final PDF will be huge, and overflow the page or column area. If too high, the image will appear quite small on the page. (Example screenshots follow).

Note that my experience with printed book content, is that these images look fine, when printed in monochrome.

Now, some print-on-demand (POD) companies, such as LuLu, would like a better DPI (300), but my experience has been positive with the 144 DPI setting. Not so many years ago, LuLu’s  printing equipment would choke and die on images in the wrong format. If you create a book

containing images using AUTHOR, be sure to get a draft from your POD company, and be sure to preview the print-format PDF on your screen, checking all images for acceptable appearance. Yes, this is a burden on the editorial staff, but it must be done in advance of selling a printed book.

This image format I’ve described seems to work fine with Kindle and EPUB as well. Most Kindle viewers allow you to zoom in on an image. The detail isn't perfect with these settings, but will meet most needs. (Note that my personal book projects have primarily illustrated postal history items. Collectors of these historical documents want to see some close detail, such as postal markings. No objections so far.)

## **Example of Using Wrong DPI Setting on Image**

The two screenshots below show a 2-column book layout PDF, as displayed on the computer screen. In one case, the image is set to 144 DPI, in the other, a much higher number. As a result of the higher DPI, the image is shrunk down by Apache FOP, during the PDF creation process. This experiment was performed to emphasize the problems that can arise, “your mileage will vary...”

[example page] (http://swansongrp.com/pics/normal_pdf_page.jpg)

*Screenshot of “normal” 2-column PDF page. Be aware of the appearance of the image marked: “Back of Cover #1”.*

[wrong_page] (http://swansongrp.com/pics/dpi1000_problem_pdf_page.jpg)

*Screenshot of 2-column PDF file, where the DPI setting of “Back of Cover #1” has been set quite high. The Apache FOP system shrunk the image in the final PDF.*

Note that this behavior is not considered a bug by the Apache FOP development team.
