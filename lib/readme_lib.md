# About the "lib" Folder Contents

(updated 5/2/2023)

The "lib" folder contains JAR files, and other support files needed for the AUTHOR system.

- The "author.jar" archive was first uploaded 12/8/2018, and updated 12/28/2018. For trying examples, a user does not need to compile the Java code, in order to run AUTHOR. Be aware, that only the CLASS files are in this JAR, that is only the compiled Java code. (Note the date of creation. There have been updates since 2018.)

- The JSON processing for Java is the "BFO" system. As of this documentation 5/2/2023, the BFO organization has moved on and expanded the product. The existing "bfojson.jar" in this folder will work fine for both compilation and runtime.

- When executing AUTHOR, there are additional JSON files required for common resource information. These files are in the "source" area, but are not compiled as such. They must be present in the execution directory when running AUTHOR. Otherwise, you will receive errors that a certain "JSON" file is missing. They are named "Creat*.json". (The reason for this design is to allow on-the-fly formatting changes to the final e-document output. If these files were embedded in the JAR, there would be much recompilation and re-packaging needed  to make small format-related changes.)


- I have included the Vanilla CSS file in this directory as it is needed when you deploy HTML output in the Vanilla (default) mode. So far 5/2/2023, any recent Vanilla CSS file will work, including version 3.

- I have included the Javascript file "utility_v.js", which is needed when you deploy HTML output in the Vanilla (default) mode, and have included local images in your document. Local image display imbeds  a "thumbnail" on the HTML page, with an option to pop up a full-size image in its own window. The Javascript performs the popup formatting, and window control.
