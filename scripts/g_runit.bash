#!/bin/bash
#
# MUST USE bash, not SH
#
# DO NOT run script with redirection, as
# the redirection of stdout and stderr
# are ALREADY present!
#
# THIS IS GENERIC, and depends on the execution
#  part of the project-specific script which is located in the sandbox/project
#  staging area
#
PROJ=${1:?Must specify project name (directories in sandbox area)}

JAVA=java

MEM=" -Xms100m -Xmx100m "
##MEM=" -Xms400m -Xmx400m "
##MEM=" -Xms200m -Xmx200m "
##MEM=

# following for JSON
CP=$CP:bfojson-1.jar
CP=$CP:author.jar
##CP=$CP:.


echo classpath= $CP

OBJ=BookCreate

#
# invoke project-specific script from project area in the sandbox
# it will indicate the input file, and create the desired
# AUTHOR output format.
#
#
# This bash script that is performing the invoke might not be
# located in the main all_publishing area, for instance if we
# are testing in a temporary work area. Thus, the pathway
# to the project-specific area is HARD-CODED! (the project-specific
# script fragment ALSO contains the pathway string.
#
# we use bash commend "." to invoke, so it will inherit the values
# we've defined here
#

PBASEDIR=/home/theswansons/Downloads/rds/UWORK/all_publishing

##ls -l $PBASEDIR/sandbox/$PROJ/
##ls -l $PBASEDIR/sandbox/
##ls -l $PBASEDIR/
##ls -l 
echo invoking  $PBASEDIR/sandbox/$PROJ/project.bash
. $PBASEDIR/sandbox/$PROJ/project.bash

