import java.util.Date;
import java.text.DateFormat;



/*
 * special key processor for debugging. We just
 * echo back the key that was used.
 *
 */

public class SpecialKeyECHO implements SpecialKeyProcessor
{
	String echo;

	/*
	 * constructor with string we echo
	 */
	public SpecialKeyECHO(String e)
	{
		echo = e;
	}

	public String replace(String val)
	{
		return echo;
	}
} // end special key echo

