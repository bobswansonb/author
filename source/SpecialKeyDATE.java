import java.util.Date;
import java.text.DateFormat;



/*
 * special key processor for DATE. Currently inserts
 * the current date in a shortened format
 *
 */

public class SpecialKeyDATE implements SpecialKeyProcessor
{
	/*
	 * no param constructor
	 * SHOULD WE PROVIDE A constructor  that tells us what kind
	 * of formatting to perform? Perhaps indicates
	 * something different from DateFormat.SHORT  ?
	 */
	public SpecialKeyDATE()
	{
	}

	public String replace(String val)
	{
		Date today = new Date(); // use date/time within second
		DateFormat form = DateFormat.getDateTimeInstance(
		DateFormat.SHORT,
		DateFormat.SHORT);
		return form.format(today); // doesn't use input string
	}
} // end special key date

