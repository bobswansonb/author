import java.util.Iterator;
import java.util.List;

// for JSON processing
// 

import com.bfo.json.Json;


public class SpecialKeyFRONT_MATTER_FOP implements SpecialKeyProcessor
{
	String delimiter = ":"; // default
	String formatter = "<!-- special formatter goes here -->"; // if nothing passed, NOT GOOD

	/*
	 * no param constructor
	 */
	public SpecialKeyFRONT_MATTER_FOP()
	{
		this("<!-- special formatter goes here -->"); // if nothing passed, NOT GOOD
	}

	/*
	 * constructor that takes the special FOP formatting string
	 * for the lines in the front matter (usually used by PDF processors, hence the name FOP)
	 */
	public SpecialKeyFRONT_MATTER_FOP(String form)
	{
		formatter = form;
	}

	public String replace(String val)
	{
		/*
		* "PROJECT_FRONT_MATTER"
		*
		* THIS MUST BE a List as made by toString() in the Json
		* object. Apparently, a simple Object like that
		* can be both read and written by Json. (nice)
		*/
		Json test = null;
		List items = null;
		try
		{
			test = Json.read(val); // can it parse its own output and tell us what it is?
			// got this far without throwing exception, we think its a List, lets see
			items = test.listValue(); // if this barfs, something is wrong inside passed info
			// DONT FORGET, the List contains Json objects, which we hope are wrapping single Strings
			System.out.println("input: " + val + ", Json sez: " + test.type());
		}
		catch (Exception ex)
		{
			// Json didnt like it, we just process it
			if (ex instanceof java.lang.IllegalArgumentException)
			{
				// expected under normal circumstances, we try to ignore (unless debugging)
			}
			else
			{
				System.out.println("input: " + val + ", Json sez error: " + ex); // we are intrigued
			}
		}
		StringBuffer result = new StringBuffer();
		if (test != null)
		{
			// Json figured it out, we appear to have  a List, nothing fancier, please
			Iterator ii = items.iterator(); // dont forget IMMUTABLE
			while (ii.hasNext())
			{
				Json jj = (Json)ii.next(); // each item is a Json that contains a string
				result.append(formatter + jj.stringValue() + "</fo:block>\n"); // one line of title page content
			} // end loop on all strings that were inside the List
			//first paragraph may need break(s) "<br/>");
		}
		else
		{
			// Json didnt like it, we process as delimited strings
			// THIS WILL BE UGLY, how about throwing an Exception??
			String xx[] = val.split(delimiter); // split on the provided delimiter
			for (int inner  = 0 ; inner < xx.length ; inner++)
			{
				result.append("<p>" + xx[inner] +
				"</p>"); // paragraph
				//first paragraph may need break(s) "<br/>");
			}  // end loop through all strings to be treated as separate lines         
		}
		return result.toString();
	} // end replace
} // end special key project front matter (FOP)

