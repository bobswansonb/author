import java.io.PrintWriter;
import java.io.InputStream;
import java.io.File;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.TreeMap;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;


// for JSON processing
//
import com.bfo.json.Json;
import com.bfo.json.JsonReadOptions;

/*
 * updated 
 * Fri 14 Aug 2020 11:19:54 AM CDT
 *
 * Use BFO Json processor, rather than Jackson
 *
 * BUG: kinda an issue, if we want ONLY the front matter,
 * won't work, because the title page code creates
 * that too. Not stand-alone. 
 *
 * 
 * Use simple "right" page layout, rather than fancy odd-even
 * (printed version will have to use odd-even)
 * 
 * Use Jackson for JSON processing (no longer!)
 * 
 * Project-Agnostic, Format-Specific  created 4/22/2017
 */

public  class CreateSpecialContentFOPPDF extends SpecialContentCreator
{
    /*
     * CURRENT NOTES:
     *
     * o) the bookmarks on the left side should start with the
     *    initial letter, because PDF viewers cut off the bookmarks 
     *    after only about 10 letters. much depends on the PDF viewer.
     *
     *
     * o) add NO POSTAL history listing to the end of the NOF.
     *    SAME for Print PDF
     *
     * o) KEYWORDS do not seem to get through to the metadata. 
     *    I can always post-insert them, but what the??
     *
     *    Looking at the OPF for EPUB, the entry seems to be dc:subject
     *    I see that listed in the OPF standard, keywords live there, BUT
     *    FOP Metadata writeup suggests keywords in a "pdf:Keyword" namespace
     *    no additional info. I don't know how to specify, have to search
     *    discussion group and/or help for FOP at Apache. (multiple dc:subject
     *    entries did not really help).
     *
     *    SAME ISSUE with Print PDF
     *
     *
     *
     * o) Experiment with single-column is SUCCESSFUL, make it the default,
     *    but allow setting for 2-column, the JSON is pretty simple. This does
     *    NOT apply to printed PDF. (1-column was over 1000 pages)
     *    LAYOUT_MASTER_FOPnnn are the boilerplate items
     *
     * The front material pattern is:
     *
     *  1) Front Cover (just an image)
     *
     *  2) Title Page (simple name, author)
     *
     *  3) Front material (ISBN, edition, dates, etc)
     *
     *  4) Preface (format-specific text on its own
     *      page
     */
    
	/*
	 * key is our name for the string group we want to write out
	 * payload is TextContent object that has the text content
	 */
	public Map g_texts = new TreeMap();


	// instance initializer
	{
		g_texts.put("static_header1", new TextContent("static_header1")); // boilerplate, not written to any particular file
		g_texts.put("static_header2", new TextContent("static_header2")); // boilerplate, not written to any particular file
		g_texts.put("page_head", new TextContent("page_head")); // these are written to the output stream 
		g_texts.put("title_page", new TextContent("title_page")); 
		g_texts.put("cover_page", new TextContent("cover_page")); 
		g_texts.put("preface_text", new TextContent("preface_text")); 
	}
    
        String[] special_keys = {
//    	"PROJECT_FRONT_MATTER" // must split into single lines
		"XX_TT_SS" // do nothing special, let title page handler do it
    	    	    };

	public Map g_special_keys_map = new TreeMap();
	/*
	 * it appears that we don't have special processing here (true?)
	 *
	 * so, we make a special keys Map but nothing in it.
	 */

	/*
	 * example of static intializer for populating the special keys map 
	{
		g_special_keys_map.put("PROJECT_TEXT",new SpecialKeyPROJECT_TEXT("::")); // try the double colon delimiter
		g_special_keys_map.put("PROJECT_DESCRIPTION", new SpecialKeyPROJECT_DESCRIPTION());
		g_special_keys_map.put("PROJECT_DATE",new SpecialKeyDATE());
	}
	*/

    /*
     * project-specific strings that are to be replaced
     *
     * key = string to search for in boilerplate
     * 
     * value = ReplacementString object that has the
     *   replacement string, and a flag to indicate special processing
     */
    TreeMap project_keys = null; 

    /*
     * repository of options that drive this object
     * created in the getMetaData method AFTER instantiation
     */
    Properties options = null;
    
    public CreateSpecialContentFOPPDF() throws Exception
    {
        /*
         * read in the data that is used by this object
         */
        Map userData = BookUtils.readJSON(
		this.getClass().getName(), false);
        // userData is a Map containing the named arrays

	/*
	 * unlike HTML, we have a single entry for "boilerplate"
	 */
	boilerplate = BookUtils.getPropertiesFromJSON(userData,"boilerplate",
		this.getClass().getName());
		// debug System.out.println("Boilerplate: " + boilerplate);
        
	// now that we have the JSON read in, we populate ALL of the arrays of strings
		Iterator inner = g_texts.values().iterator(); // we don't care about the keys right now
		while (inner.hasNext())
		{
			TextContent content = (TextContent)inner.next();
			content.create(userData); // get text content from JSON
		}
		// already have Json object, just get the boilerplate group and make JsonProperties
		// that is stored globally in key_values
		// 
		 /*
		 * following are PROJECT-specific set of keyword/values
		 */
		key_values = BookUtils.getPropertiesFromJSON("project.json",
			"project");
		// debug System.out.println("project-specific key_values: " + key_values);

		options = BookUtils.getPropertiesFromJSON("options.json",
		"format_options"); 

	} // end instantiation
    
    public void createTitlePage(Object out, Object notused) throws Exception
    {
        PrintWriter pr = (PrintWriter)out; // this cast HAS TO WORK

	// this PDF style, NO COVER PAGE (this may not be the best approach)

	/*
	 * If no title,cover, or preface pages are wanted we DO NOT
	 * enclose them in a page-sequence!
	 * 
	 * ALL of these are OPTIONAL 
	 */
	if ( (g_options.wantPrefacePage()) ||
	 (g_options.wantCoverPage()) ||
	 (g_options.wantTitlePage()) )
	{
		// START PAGE-SEQUENCE
		pr.print(gT("TITLE_PAGE_SEQ_FOP"));
		// no page number at bottom
		// no title with links at top
		// START FLOW
		pr.print(gT("FLOW_START_FOP"));
	}
	else
	{
		return; // NOTHING TO DO (don't make it any worse)
	}
        /*
	 * THESE ARE OPTIONAL
         * step 1: cover page (split it out)
         * step 2: title page and front material (BOTH)
         * step 3: preface for PDF (and print) users
         */
        /*
         * title_page_object is the "title_page" in JSON
         * as a set of strings, to be printed. If there is
         * an embedded array, the first entry in that
         * array is the FORMATTING code, the second contains
         * all the rest of the content, separated by colons (:)
         *  
         * "cover_page_object" on the other hand is just a simple String List  
         */

        Object someobject = null;
        Iterator ii = null;

	TextContent contentx =  null;

	if (g_options.wantCoverPage())
	{
	
		contentx =  (TextContent)g_texts.get("cover_page");
		BookUtils.createAPageWithModifications(pr,contentx,key_values,g_special_keys_map,false); // no close
	}

	if (g_options.wantTitlePage())
	{
		/*
		 * This is somewhat messy. What we have is a bunch of FOP boilerplate
		 * that has keyword fill-ins (title). Then, we follow with the front matter (new page).
		 * THAT needs to use a special FOP formatter that is used for every line. That
		 * probably can be dug out of the Properties, it probably doesn't need to be
		 * in the keyword replacement. The front matter otherwise is a series of lines.
		 * Should probably be a List in the JSON
		 */
		// we force in special processor, it might overwrite another, but I don't think so
		g_special_keys_map.put("PROJECT_FRONT_MATTER",new SpecialKeyFRONT_MATTER_FOP(
					gT("FRONT_MATTER_FORMAT"))); // pass the formatter for every line of the FRONT MATTER
		contentx =  (TextContent)g_texts.get("title_page");
		BookUtils.createAPageWithModifications(pr,contentx,key_values,g_special_keys_map,false); // no close
		
	}  // end if want title page
        /*
         * OK, we handled the preface differently, we can make a special
	 * replacer for it. (in the past, the first line was a TITLE, and
	 * formatted differently.) For NOW! we just send it out
         */
	if (g_options.wantPrefacePage())
	{
		contentx =  (TextContent)g_texts.get("preface_text");
		BookUtils.createAPageWithModifications(pr,contentx,key_values,g_special_keys_map,false); // no close
	/* old TITLE handling...
		String the_title = (String)someobject;
		pr.print("<fo:block page-break-before=\"always\" font-size=\"32pt\"\nfont-weight=\"bold\"\nspace-after.optimum=\"10pt\"\nspace-before.optimum=\"0\"\ntext-align=\"center\">" +
			 the_title +
			 gT("STATE_TITLE_BLOCK_FOP_END"));
			 */
        
		/* will NEED THIS TO MAKE THE WHOLE THING WORK
			pr.print(gT("NORMAL_BLOCK_FOP") +
				 someobject +
				 gT("NORMAL_BLOCK_FOP_END"));
			continue; // done for now
			*/
	} // end if want preface page

	/*
	 * based on logic above, SOMETHING got
	 * printed, title and/or preface
	 * SOOOO we must terminate the flow and page-sequence
	 */
        
        pr.print(gT("FLOW_END_FOP"));
        endPageSeq(pr); // end page-sequence for title/preface
    } // end create title page, and preface, if wanted
    
    
    public void createMetadata(Object out) throws Exception
    {
        PrintWriter pr = (PrintWriter)out; // this cast HAS TO WORK
	// ALL of the needed content for the FOP metadata is in the JSON content
	TextContent contentx =  (TextContent)g_texts.get("page_head");
	BookUtils.createAPageWithModifications(pr,contentx,key_values,g_special_keys_map,false); // no close
    } // end create metadata
    
    public void createStaticHeaders(Object out, Object notused,
		AuxiliaryInformation aux) throws Exception
    {
		createStaticHeaders(out,notused); // no pass through of aux
	}
    public void createStaticHeaders(Object out, Object notused) throws Exception
    {
        PrintWriter pr = (PrintWriter)out; // this cast HAS TO WORK
        
	/*
	 * static stuff is broken into 2 parts, those BEFORE
	 * the page top links, and those after 
	 * 
	 * we create the links HERE, because the user may not want them 
	 */
	TextContent contentx =  (TextContent)g_texts.get("static_header1");
	BookUtils.createAPageWithModifications(pr,contentx,key_values,g_special_keys_map,false); // no close
	// make links, if desired
	if (g_options.wantTOC())
	{
		pr.println(gT("TOC_LINK_FOP"));
	}
	if (g_options.wantAnyIndex())
	{
		pr.println(gT("INDEX_LINK_FOP"));
	}
/*
        "<fo:basic-link internal-destination=\"_toc\" border-bottom-color=\"#c6deff\" border-bottom-style=\"solid\" border-bottom-width=\"medium\">   [ TOC </fo:basic-link>\n",
        "<fo:basic-link internal-destination=\"_indexes\" border-bottom-color=\"#c6deff\" border-bottom-style=\"solid\" border-bottom-width=\"medium\">   Index ]</fo:basic-link>\n",
 */
	contentx =  (TextContent)g_texts.get("static_header2");
	BookUtils.createAPageWithModifications(pr,contentx,key_values,g_special_keys_map,false); // no close
        
    } // end create static headers
    
    public void renderIndex(PrintWriter pr, List all, int level) throws Exception
    {
        //    System.out.println(all);
        pr.print(gT("FLOW_END_FOP")); // end current document flow
        endPageSeq(pr);
            
        startFlow(pr,"Index"); // start a new flow with different page number indicator
        /*
         * check type of object at the top of this List, given the
         * design it MUST be a Group, as are all in this highest
         * level list. Deeper down, some groups have only
         * entries, others have more groups under them
         */
        Object working = all.get(0); // type is very important
        if (working instanceof IndexEntry)
        {
            throw new Exception("Index Structure Wrong: " + working); // NOT HERE
        }
        else
        {
            if (working instanceof IndexGroup)
            {
                /*
                 * BREADCRUMBS needed for the interactive PDF
                 * ONLY if there is more than one index type
                 */
		if (g_options.wantGeneralIndexONLY())
		{
			// do NOT make a heading/breadcrumb page!
                         // BUT NOTE that we will need an ID="_indexes" later
                         // it's gonna be ugly
		}
		else
		{
			// more than one index type wanted, make breadcrumb page
                /*
                 * HOW do we know where to drop breadcrumbs? Well, the first-level
                 * objects will all be IndexGroup objects and we will make
                 * breadcrumbs for THEM ONLY (no recursion, etc, etc)
                 */
                /*
                 * Loop through all in the list, creating breadcrumbs
                 */
                Iterator ingrp = all.iterator();
                IndexGroup inner_group = null;
		/*
		 * first block has the special ID of "_indexes"
		 * this is the MASTER location of the top of all
		 * indexes, and is pointed to by such items as
		 * the small link at the top of each page, and
		 * by the index link in the TOC
		 */
                pr.print(gT("STATE_TITLE_BLOCK_FOP1") +
                         "_indexes" + 
                         gT("STATE_TITLE_BLOCK_FOP2") +
                         "Document Indexes" +
                         gT("STATE_TITLE_BLOCK_FOP_END")); 
                while (ingrp.hasNext())
                {
                    inner_group = (IndexGroup)ingrp.next(); // MUST WORK or throw exception for bad structure
                    pr.print(gT("NORMAL_BLOCK_FOP_REST")); // start text block for all breadcrumbs
                
                    pr.print(
                        gT("LINK_FOP1") +
                        inner_group.id +
                        gT("LINK_FOP2") +
                        inner_group.short_title +
                        gT("LINK_END")); //  + " -- "); //  + NORMAL_BLOCK_FOP_END);
                    pr.print(gT("NORMAL_BLOCK_FOP_END")); // terminate breadcrumbs block
                } // end if putting in bread crumb for a particular top-level group
		} // end if breadcrumb page wanted
            
                /*
                 * NOW, we create the index, based on this structure
                 */
                renderIndexGroupList(pr,all,1); // probably will recurse
            } // end if the right kind of group
            else
            {
                throw new Exception("Index Structure Wrong: " + working);
            }
        } // end not index entry
    } // end render the indexes into FOP
    
    public void renderIndexList(PrintWriter pr, List all) throws Exception
    {
        IndexEntry the_entry = null;
        /*
         * ALL items in list are Index contents (no higher up container objects)
         */
        // debugging System.out.println("Handling index list, size: " + all.size() + ", first item: " +
         // debugging                   all.get(0));
        // debugging System.out.flush();
        Iterator inds = all.iterator();
        while (inds.hasNext())
        {
            the_entry = (IndexEntry)inds.next(); // HAS TO BE THIS TYPE, otherwise bad design
            pr.print(gT("NORMAL_BLOCK_FOP_REST") +
                          gT("LINK_FOP1") +
                         the_entry.id +    // id jump
                     gT("LINK_FOP2") +  // makes a blue-underlined link
                     the_entry.long_title +
                        gT("LINK_END") +
                   "   (" +
                   gT("PAGE_NUMBER_LINK") +
                  the_entry.id +
                   gT("PAGE_NUMBER_LINK_END") +                         
")" +
                     gT("NORMAL_BLOCK_FOP_END"));
        }
    } // end render only a list of index items

    /*
     * for interactive PDF we add breadcrumbs
     */
    public void renderIndexGroupList(PrintWriter pr, List all, int level) throws Exception
    {
        IndexGroup inner_group = null;
        Object working = null;
        Iterator ingrp = all.iterator();
        while (ingrp.hasNext())
        {
            inner_group = (IndexGroup)ingrp.next(); // MUST WORK or throw exception for bad structure
            // debugging System.out.println("Handling Group: " + inner_group.long_title);
 // debugging            System.out.flush();
            //            pr.print(gT("HEADING1_BLOCK_FOP") +
            /*
             * depending on level of index, we will do a page break.
             * Level 2 and DEEPER will not perform a page break 
            */
            if (level >= 2)
            {
                pr.print(gT("STATE_TITLE_NONBREAKING_BLOCK_FOP1") +
                         inner_group.id +
                         gT("STATE_TITLE_BLOCK_FOP2") +
                         inner_group.long_title +   // sub heading is the longer name with explanatory front, such as "facilities"
// short title does NOT include explanatory front, such as "cities"    inner_group.short_title +   // sub heading is the shorter name
                         gT("STATE_TITLE_BLOCK_FOP_END")); // separate header
            } // end level is 2 or deeper
            else
            {
                /* first level index, cause a page break, make heading */
                /* 
                 * HOWEVER, there are problems with general index-only 
                 * situations. Since there is no breadcrumb page,
                 * we are missing the necessary id=_indexes. Nasty.
                 * 
                 * Soooo, we will make two blocks, one page-break, 
                 * empty with id=_indexes. Second block will be
                 * general title, no  page-break, with id=_general_index.
                 */ 
		if ( (g_options.wantGeneralIndexONLY()) &&
			(inner_group.id.equals("_general_index")) )
		{
			// #1: empty page break block id = "_indexes"
			pr.print(gT("STATE_TITLE_BLOCK_FOP1") +
				 "_indexes" +
				 gT("STATE_TITLE_BLOCK_FOP2") +
				 gT("STATE_TITLE_BLOCK_FOP_END")); // empty header
			// #2: desired heading with correct id, NO break
			pr.print(gT("STATE_TITLE_NONBREAKING_BLOCK_FOP1") +
				 inner_group.id + // _general_index
				 gT("STATE_TITLE_BLOCK_FOP2") +
				 inner_group.long_title +  // General Index
				 gT("STATE_TITLE_BLOCK_FOP_END")); // separate header
		} // end SPECIAL CASE headings for general index
		else
		{
			pr.print(gT("STATE_TITLE_BLOCK_FOP1") +
				 inner_group.id +
				 gT("STATE_TITLE_BLOCK_FOP2") +
				 inner_group.long_title + 
				 //                     inner_group.long_title + " [" + level + "] " +  // debugging for level number
				 gT("STATE_TITLE_BLOCK_FOP_END")); // separate header
		    } // end not special case
	    } // end first-level index
            /*
             * now, check to see what kind of objects are the children
             */
            working = inner_group.children.get(0);
            if (working instanceof IndexEntry)
            {
                renderIndexList(pr,inner_group.children);
                // done, now go to next item in group list
            }
            else
            {
                if (working instanceof IndexGroup)
                {
                    // have to recurse
                    renderIndexGroupList(pr,inner_group.children,level + 1); // will recurse
                }
                else
                {
                    throw new Exception("Wrong types mixed in index groups: " + working);
                }
            }
            // call out bottom after having handled either another imbedded group or a list of index items
            //     pr.print(gT("FLOW_END_FOP"));  // terminate the higher-level group 
        } // end for each group in the list
    } // end traverse and render a group of indexes (recursive)
    
    public void endPageSeq(
        PrintWriter pr)
    {
        pr.print("</fo:page-sequence>\n");
    }

    public void startFlow(
        PrintWriter pr,
        String page_number_string) throws Exception
    {
        pr.print("<fo:page-sequence  initial-page-number=\"1\" id=\"N2528\" master-reference=\"right\">\n"); // use right only
        // pr.print("<fo:page-sequence  initial-page-number=\"1\" id=\"N2528\" master-reference=\"psmOddEven\">\n"); use odd-even layout system
        pr.print("<!-- page number at bottom --><fo:static-content flow-name=\"xsl-region-after\">\n");
        // inherit        pr.print("<fo:block text-align-last=\"center\" font-family=\"Helvetica\" color=\"gray\" font-size=\"10pt\">\n");
        pr.print("<fo:block text-align-last=\"center\" color=\"gray\" space-before.optimum=\"0\">\n");
        pr.print(page_number_string + " - <fo:page-number/>\n");
        pr.print("</fo:block>\n");
        pr.print("</fo:static-content>\n");
        createStaticHeaders(pr);
        pr.print("<fo:flow flow-name=\"xsl-region-body\">\n");
    } // end startFlow
    
    
    public void renderBookmarkIndexList(PrintWriter pr, List all) throws Exception
    {
        IndexEntry the_entry = null;
        /*
         * ALL items in list are Index contents (no higher up container objects)
         */
        // debugging System.out.println("Handling index list, size: " + all.size() + ", first item: " +
                    // debugging        all.get(0));
        // debugging System.out.flush();
        Iterator inds = all.iterator();
        while (inds.hasNext())
        {
            the_entry = (IndexEntry)inds.next(); // HAS TO BE THIS TYPE, otherwise bad design
            pr.print(gT("BOOKMARKS_LINK_FOP") +
                     the_entry.id +
                     gT("BOOKMARKS_END_FOP"));
            pr.print(gT("BOOKMARKS_TITLE_FOP") +
                     the_entry.long_title +
                     gT("BOOKMARKS_TITLE_END_FOP"));
            pr.print(gT("BOOKMARKS_BOOKMARK_END_FOP")); // end a single bookmark
        }
    } // end render only a list of index items

    public void renderBookmarkGroupList(PrintWriter pr, List all) throws Exception
    {
        IndexGroup inner_group = null;
        Object working = null;
        Iterator ingrp = all.iterator();
        while (ingrp.hasNext())
        {
            inner_group = (IndexGroup)ingrp.next(); // MUST WORK or throw exception for bad structure
            // debugging System.out.println("Handling Group: " + inner_group.long_title);
 // debugging            System.out.flush();
            pr.println(gT("BOOKMARKS_DUMMY_FOP"));
            pr.println(gT("BOOKMARKS_TITLE_FOP1") + 
                       inner_group.short_title + gT("BOOKMARKS_TITLE_FOP2")); // unterminated at this point
//            inner_group.long_title + gT("BOOKMARKS_TITLE_FOP2")); // unterminated at this point
            /*
             * now, check to see what kind of objects are the children
             */
            working = inner_group.children.get(0);
            if (working instanceof IndexEntry)
            {
                renderBookmarkIndexList(pr,inner_group.children);
                // done, now go to next item in group list
            }
            else
            {
                if (working instanceof IndexGroup)
                {
                    // have to recurse
                    renderBookmarkGroupList(pr,inner_group.children); // will recurse
                }
                else
                {
                    throw new Exception("Wrong types mixed in index groups: " + working);
                }
            }
            // call out bottom after having handled either another imbedded group or a list of index items
            pr.print(gT("BOOKMARKS_BOOKMARK_END_FOP"));  // terminate the higher-level group bookmark
        } // end for each group in the list
   //     pr.print(gT("BOOKMARKS_BOOKMARK_END_FOP")); // terminate higher level bookmark
    } // end traverse and render a group of bookmarks (recursive)
    
    public void renderBookmarks(PrintWriter pr, List all) throws Exception
    {
    //    System.out.println(all);
        pr.println(gT("BOOKMARKS_FOP1")); // start bookmark tree
        /*
         * check type of object at the top of this List, given the
         * design it MUST be a Group, as are all in this highest
         * level list. Deeper down, some groups have only
         * entries, others have more groups under them
         */
        Object working = all.get(0); // type is very important
        // debugging System.out.println("Entering bookmark renderer, object: " + working);
 // debugging        System.out.flush();
        if (working instanceof IndexEntry)
        {
            throw new Exception("Index Structure Wrong: " + working); // NOT HERE
        }
        else
        {
            if (working instanceof IndexGroup)
            {
                renderBookmarkGroupList(pr,all); // may recurse
                // we are done having recursed through all groups
            } // end another group under the main one
            else
            {
                throw new Exception("Index Structure Wrong: " + working);
            }
        } // end not index entry
        /*
         * at this point, we have rendered all groups and entries under them, tree done
         */
        pr.print(gT("BOOKMARKS_TREE_END_FOP"));  
    } // end render the bookmarks into FOP
    
    public void createTOC(PrintWriter pr,
                          Map all_maps,
                          List appendixes,
                          Map index_flags) throws Exception
    {
        /*
         * unwrap the maps and pass to some code that is very
         * specific to this book and format. Exceptions thrown
         * from here will be prefixed with "TOC key missing:".
         */
        makeTableOfContents(pr,
                            get_a_map("STATE",all_maps,"TOC"),
                            get_a_map("ABBREVIATIONS",all_maps,"TOC"),
                            get_a_map("CITY",all_maps,"TOC"),
                            get_a_map("FACILITY",all_maps,"TOC"),
                            get_a_map("GENERAL",all_maps,"TOC"),
                            appendixes,
                            index_flags);
    }
    

    /*
     * code taken from the fop pdf sink
     */
    public void makeTableOfContents(PrintWriter pr,
                                    Map state_index_map,
                                    Map abbrev_map,
                                    Map city_index_map,
                                    Map fac_index_map,
                                    Map general_index_map,
                                    List appendixes,
                                    Map index_flags
                                    ) throws Exception
    {
	/*
	 * TOC is OPTIONAL
	 */
	if (g_options.wantTOC())
	{
		startFlow(pr,"TOC"); // start the TOC flow
		pr.print(gT("STATE_TITLE_BLOCK_FOP1") +
	"_toc" +
			 gT("STATE_TITLE_BLOCK_FOP2") +
			 gT("MARKER_START_STATE_FOP") +
			 "TOC" +
			 gT("MARKER_END_FOP") +
			"Table of Contents" +
			 gT("STATE_TITLE_BLOCK_FOP_END"));
		
		Iterator states = state_index_map.keySet().iterator();
		ExactReferenceComparable the_ref = null;
		List some_items = null;
		String index_item = "";
		/*
		 * first, the introduction
		 */
		pr.print(makeTOCLine(
			 "_intro", 
			"Introduction",""));
        
		while (states.hasNext())
		{
		    index_item = (String)states.next();  // state name
		    // ONLY ONE STATE PER ENTRY
		    some_items = (List)state_index_map.get(index_item);
		    the_ref = new ExactReferenceComparable((ExactReference)some_items.get(0)); // first item only
		    
		    // we are only processing state items
			pr.print(makeTOCLine(
			     "state_" + 
			     BookUtils.eC(the_ref.state_name),   // escape state name
			     index_item,""));

		} // end for each state toc item
        
		Iterator appit = appendixes.iterator();
		String app_name = "";
		String app_title = "";
		
		while (appit.hasNext())
		{
		    app_name = (String)appit.next();
		    app_title = (String)appit.next(); // bad boy!
			pr.print(makeTOCLine(
				app_name,
				app_title,""));
		} // end list all appendixes in TOC
		/*
		 * point to indexes. 
		 */
		// HERE the general index handling should allow for optional!
		if (g_options.wantGeneralIndex())
		{
		    pr.print(makeTOCLine(
			 gT("GENERAL_INDEX_ID") ,
			 gT("GENERAL_INDEX_TITLE"),
			"Index - " ));
		}
		if (index_flags.containsKey( "INDEX_STATES"))
		{
		    pr.print(makeTOCLine(
			     gT("STATE_INDEX_ID"),
			     gT("STATE_INDEX_TITLE"),
			"Index - " ));
		} // end if putting in TOC entry for state index
            
		if (index_flags.containsKey( "INDEX_CITIES"))
		{
		    pr.print(makeTOCLine(
			     gT("CITY_INDEX_ID"),
			     gT("CITY_INDEX_TITLE"),
			"Index - " ));
		} // end write TOC entry for city index
            
		if (index_flags.containsKey( "INDEX_FACILITIES"))
		{
		    pr.print(makeTOCLine(
			     gT("FACILITY_INDEX_ID"),
			     gT("FACILITY_INDEX_TITLE"),
			"Index - " ));
		} // end breadcrumb for facility index
            
		if (index_flags.containsKey( "INDEX_NO_POSTAL_HISTORY"))
		{
		    pr.print(makeTOCLine(
			     gT("FACILITY_NO_INDEX_ID"),
			     gT("FACILITY_NO_INDEX_TITLE"),
			"Index - " ));
		} // end TOC entry for no postal history facilities
        
        /*
         * KLUGE: the TOC is really only one page long. However, the
         * layout for PDF is left-right. We want to maintain that
         * layout, so we need to add an empty page. We'll make
         * another page with (this page left intentionally blank) on it.
         */
// NOT AT THIS TIME        pr.print(gT("BLANK_PAGE_FOP"));
        
	/*
	 * we created some block content above, because TOC
	 * was wanted. we need to terminate the flow, and allow for
	 * next part of the document.
	 */
		pr.print(gT("FLOW_END_FOP") + " <!-- end of flow for toc -->\n");
		endPageSeq(pr);
	} // end if want a TOC
    } // end make table of contents

// HERE HERE, we need to make introduction NOT a fixed, separate section
//	public boolean wantIntroductionByDefault()

	public String makeTOCLine(String id, String narrative,
		String page_number_pre) throws Exception
	{
		return
			gT("NORMAL_BLOCK_FOP_REST") +
			gT("LINK_FOP1") +
			id +
			gT("LINK_FOP2") +
			narrative  +
			gT("LINK_END") +
			"   (" +
			page_number_pre + // can be empty string
			gT("PAGE_NUMBER_LINK") +
			id +
			gT("PAGE_NUMBER_LINK_END") +
			")" +
			gT("NORMAL_BLOCK_FOP_END");
	} // end make simple TOC line

	/*
	 * this code is unique to this format-specific object
	 * each switch() position is based on the special_key
	 * position.
	 * 
	 * return a String that will be pushed back into the
	 * JSON structure, as a complete replacement
	 * for the original string in which the special key was
	 * found
	 */
	public String specialReplacementProcessing(ReplacementString rval)
	{
		StringBuffer result = new StringBuffer();
		switch (rval.flag)
		{
			case 0:
			{
			    	// "PROJECT_FRONT_MATTER"
				//result.append("<h1>"); // front is a heading WHAT TO DO?
			    	String xx[] = rval.rep.split(":"); // 1 (one) colon delimiter
        			for (int inner  = 0 ; inner < xx.length ; inner++)
        			{
        				result.append(xx[inner] +
        					"\n\n" ); // skip two lines
        				if (inner < 0)   // NOT USED first line only
        				{
        					result.append("</h1>" +
						"<p style=\"text-align: center;\">" +
						"<br/>");

        				} // end first line only
				}  // end loop through all strings to be treated as separate lines         
			//	result.append("</p>"); HOW TO END?
			    	break;
			} // end 0 which is PROJECT_FRONT_MATTER
			case 1:
			{
			    	// "PROJECT_KEYWORDS"
			    	String xx[] = rval.rep.split(":"); // 1 (one) colon delimiter
        			for (int inner  = 0 ; inner < xx.length ; inner++)
        			{
        				result.append(xx[inner] +
        					","); // keywords are comma-delimited
				}  // end loop through all strings to be treated as separate lines         
				result.append("web"); // dummy for end
			    	break;
			} // end 1 which is PROJECT_KEYWORDS
			case 2:
			{
			    	// "PROJECT_COPYRIGHT"
				result.append( "<meta name=\"copyright\" content=\"" + 
			    	rval.rep + "\"/>");
			    	break;
			} // end 2 which is PROJECT_COPYRIGHT
		} // end switch on special code segments
		return result.toString();		
	}	// end specialReplacementProcessing
	
	/*
	 * get Options information from the project json file, as well
	 */
	public Object getMetaData(String json_file) throws Exception
	{
		Object result = getProjectSpecificMetaData(json_file); // use helper in base class
		/*
		 * NOW, read json file for PDF-specific! OPTIONS we will use
		 */

		// now that we processed the options, return the metadata info
		return result;
	} // end getmetadata (overrides parent class)


    
    public void renderNOFChecklist(PrintWriter pr, Map nof_by_city) throws Exception
    {
    }

    public String getProjectKeyValue(String xx) throws Exception
    {
		throw new Exception("FOP (PDF) sink does not use project values");
	}
   
	/*
	 * only property we process right now is column count
	 * 
	 * we tried to change orientation (portrait vs landscape). THIS IS
	 * DONE in the configuration file for the FOP command,
	 * completely outside this Java program. 
	 */
	public String getProperty(String key)
	{
	    if (key.equals("PDF_COLUMN_COUNT"))
		{
			return options.getProperty(key,"1"); // default 1
		}
		else
		{
			return null;
		}
	} // end getproperty
	
    
} // end special content creator facilities  FOP format for PDF output only (not print)

