import java.io.PrintWriter;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Map;
import java.util.Iterator;
import java.util.TreeMap;
import java.util.List;
import java.util.ArrayDeque;

// for JSON processing

import com.bfo.json.Json;
import com.bfo.json.JsonReadOptions;


/*
 * modified 
 * Mon 10 Aug 2020 05:53:27 AM CDT
 *
 * encapsulates a "text" array containing
 * important content that will be written out
 * by AUTHOR Sinks
 *
 * The "text" in this case, comes from Json files
 * and is SPECIFIC to HTML creation. There is an additional
 * layer of specification, where an object can take on
 * different "flavors" such as VANDARK
 */

public class HTMLTextContent extends TextContent
{
	// everything else is inherited
	//
	
	/*
	 * no param constructor
	 */
	public HTMLTextContent()
	{
		super();
	}

	public HTMLTextContent(String t)
	{
		super();
		tag = t;
		output = null;
	}

	public HTMLTextContent(String t, String fi)
	{
		super();
		tag = t;
		output = new File(fi); // throws excpetion?
	}

	/*
	 * the Map passed contains Json objects
	 */
	public void create(Map source, String type) throws IllegalArgumentException
	{
		Json jj = (Json)source.get(tag); // top-level structure within the map for the type of string grouping we want
		if (!jj.isMap())
		{
			throw new IllegalArgumentException("Item: " + tag + " is not present in: " + source);
		}
		Map jmap = jj.mapValue(); // this is a Map, because there are keyed markers for each type
		Json subgroup = (Json)jmap.get(type); // note that Map contents are Json object (list of strings)
		text = JsonUtils.getStrings(subgroup); // use helper to populate our array
	}

} // end HTMLtextcontent encapsulation

