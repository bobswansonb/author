import java.io.*;

import java.util.Properties;
import java.util.Enumeration;
import java.util.Map;
import java.util.TreeMap;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Arrays;
import java.util.Date;

import java.text.DateFormat;

import java.util.zip.*;





/*
 * Last updated 4/21/2017
 *
 * A program to read JSON that contains Java programs,
 * and to customize those programs for the book
 * creation system. The programs are largely identical,
 * but the metadata is the part that we modify.
   */

public class MetaCreator
{

    private String project;
    
    private PrintWriter g_pr;

	private Object  java_content = null;
	
	private Object boilerplate_content = null;
	
	private String printed_date = "nothing";
	
	public TreeMap key_values = null; // will be filled from JSON boilerplate



    public static void main(String args[]) {
    	        try
        {
            MetaCreator t = new MetaCreator();
            t.init(args);
            t.execute();
            t.finish();
        } // end try
        catch (Exception trouble)
        {
            System.err.println("problem: " + trouble);
            trouble.printStackTrace();
        }
    }

    public MetaCreator()
    {
    }

    public void init(String args[]) throws Exception
    {
		if (args.length < 3)
		{
			throw new Exception("Too Few Args: " + args);
		}
		String input_json = args[0];
		String output_java = args[1];
		 project = args[2];
		 
		 g_pr = new PrintWriter(new FileWriter(new File(output_java)));
		 
		
        /*
         * read in the data that is used by this object
         */
        InputStream ins = ClassLoader.getSystemResourceAsStream(input_json); // if no specification, will be bad name
         

        Object yy = JSONValue.parseWithException(new InputStreamReader(ins)); // parse the JSON stuff

        if (yy instanceof JSONObject)
        {
            Map mapper = (Map)yy;
            /*
             *  These structures are VERY simple
             * for our needs:
             *
             */
             // java code first
		 java_content = mapper.get("JavaContent");
            if (!(java_content instanceof JSONArray))
            {
                throw new Exception("Problems with JSON, JavaContent : " + 
                java_content.getClass().getName());
            }
            // boilerplate second
            populate_key_values(mapper);
//		 boilerplate_content = mapper.get("boilerplate");
  //          if (!(boilerplate_content instanceof JSONArray))
    //        {
      //          throw new Exception("Problems with JSON, boilerplate : " + 
        //        java_content.getClass().getName());
          //  }
 		}
        else
        {
            throw new Exception("Problems with JSON: " + yy.getClass().getName());
        }
        
 	
		/*
		 * done with JSON processing, all data should now be in structures
		 */
		 

        		Date today = new Date(); // use date/time within second
		DateFormat form = DateFormat.getDateTimeInstance(
			DateFormat.SHORT,
			DateFormat.SHORT);
		 printed_date = form.format(today);
		/*
		 * clean up for filename
		 */
	//	printed_date = printed_date.replace("/","_");
	//	printed_date = printed_date.replace(":","_");
	//	printed_date = printed_date.replace(" ","_");

   } // end init
   
   

    public void populate_key_values(Map mapper) throws Exception
    {
        Object key_values_strings = mapper.get("boilerplate");
        key_values = new TreeMap(); // empty treemap to start
        if (key_values_strings == null)
        {
            System.out.println("Boilerplate strings do not exist.");
            return;  // return with empty treemap
        }
        if (!(key_values_strings instanceof JSONArray))
        {
            throw new Exception("Problems with JSON, boilerplate : " + 
				key_values_strings.getClass().getName());
        }
        /* 
         * the array must be even, each odd item is key, even
         * value is the value. These are stored in the Map
         */
        Object someobject = null;
        String key = "";
        JSONArray arr = (JSONArray)key_values_strings;
        System.out.println("Boilerplate strings:" + arr.size());
        if (arr.size() %2 != 0)
        {
            throw new Exception("Problems with JSON, boilerplate list is not even sized: " + 
                                arr.size());
        }
        for (int ii = 0 ; ii < arr.size() ; ii += 2)
        {
            someobject = arr.get(ii);
            if (!(someobject instanceof String))
            {
                throw new Exception("Problem with boilerplate at: " + ii + 
                                    " not string: " + someobject);
            }
            key = (String)someobject; // will be key
            someobject = arr.get(ii + 1);
            if (!(someobject instanceof String))
            {
                throw new Exception("Problem with boilerplate at: " + (ii+1) + 
                                    " not string: " + someobject);
            }
            key_values.put(key,(String)someobject); // populate the key value pairs
        } // end loop on the strings in boilerplate
        System.out.println("KeyValues: " + key_values);  // debugging
    } // end populate the key-value pairs

   public void execute() throws Exception
   {
   
        Object someobject = null;
//
// java_content is  an array of Strings
//
        Iterator ii = ((JSONArray)java_content).iterator();
        while (ii.hasNext())
        {	
            someobject =  ii.next();
            if (someobject instanceof String)
            {
            	String work1 = (String)someobject; // cast
            	String work2 = work1.replace("CREATE_DATE",printed_date); // change comment with date
            	String work3 = work2.replace("BOOK_PROJECT",project); // change object names for project
            	g_pr.println(work3);
            }
        	else
        	{
		    throw new Exception("Problems with JSON inside: " + 
		    java_content + ", object: " + 
		    someobject.getClass().getName());
		  }
		  } // end loop on all strings


    } // end execute

	public void finish() throws Exception
	{
			g_pr.close(); // close output stream
	}

} // end metacreator
