import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Writer;
import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.TreeMap;
import java.util.Map;
import java.util.Date;
import java.text.DateFormat;

/*
 * last edited 
 * Tue 11 Aug 2020 08:44:30 AM CDT
 */

public class DebugPrintWriter extends PrintWriter
{
	public String name;
	public String comments;
	public String paren;

	public DebugPrintWriter(Writer wr, String na, String co)
	{
		super(wr); // invoke PrintWriter and all its stuff
		name = na;
		comments = co;
		paren = wr.getClass().getName();
	}

	public String toString()
	{
		return "Debugging " + name + ", comments: " + comments + ", for object: " + paren;
	}
} // end debug printwriter to help with debugging
