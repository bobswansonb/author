import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.File;
import java.io.FileInputStream;

import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.TreeMap;
import java.util.Calendar;

import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import javax.imageio.ImageIO;

import XMLUtil.DOM4JAdapter;


import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.PDDestinationNameTreeNode;
import org.apache.pdfbox.pdmodel.PDDocumentNameDictionary;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.font.PDSimpleFont;
import org.apache.pdfbox.pdmodel.font.PDTrueTypeFont;
import org.apache.pdfbox.pdmodel.graphics.color.PDGamma;
import org.apache.pdfbox.pdmodel.graphics.color.PDDeviceGray;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDJpeg;
import org.apache.pdfbox.pdmodel.interactive.action.type.PDActionURI;
import org.apache.pdfbox.pdmodel.interactive.action.type.PDActionGoTo;
import org.apache.pdfbox.pdmodel.interactive.action.type.PDAction;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotationLine;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotationSquareCircle;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotationTextMarkup;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotationLink;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDBorderStyleDictionary;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.destination.PDPageXYZDestination;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.destination.PDPageDestination;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.destination.PDNamedDestination;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.destination.PDDestination;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.outline.PDDocumentOutline;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.outline.PDOutlineItem;

    
    /*
     * The MyFontFactory object attempts to make the creation
     * of the necessary fonts for PDF creation into
     * a "factory" (so to speak).
     *
     * This object will return appropriate fonts and
     * font sizes when requested, so the using
     * PDF creator does not need to know how these
     * magical things were done, nor does the owner
     * need to set any flag besides the TTF or TYPE1
     * entry.
     *
     * THIS IS A SINGLETON
     */
    public class MyFontFactory
    {
    /*
    The PostScript names of 14 Type 1 fonts, known as the standard 14 fonts, are as follows: 
    Times-Roman, 
    Helvetica, 
    Courier, 
    Symbol, 
    Times-Bold, 
    Helvetica-Bold, 
    Courier-Bold, 
    ZapfDingbats, 
    Times-Italic, 
    Helvetica-Oblique, 
    Courier-Oblique, 
    Times-BoldItalic, 
    Helvetica-BoldOblique, 
    Courier-BoldOblique
*/
        private static final int TTF = 0;
        private static final int TYPE1 = 1;
        
        public static final int FONT_TEXT  = 0;
        public static final int FONT_TEXT_BOLD  = 1; // headings of various kinds, and emphasized text
        public static final int FONT_CAPTION  = 2; // photo captions
        public static final int FONT_DETAILS  = 3; // special detail lines
        public static final int FONT_PRINTER_SYMBOLS  = 4; // currently ONLY used in headers (type 5)
        public static final int FONT_ITALIC = 5; // orginary font, but italic
        
        private static final int total_fonts = 6;  // ADJUST when fonts added
        
        private int font_type;
        private AFont[] all_heads;
        private  int[] head_font_sizes =
        {10,14,12,10,12,12}; // subscript 1,2,3
        private  float[] head_font_heights =
        {0,0,0,0,0,0}; // subscript 1,2,3
        /*
         * holds the fonts. when someone asks for
         * a font, they must specify the type (main, bold, caption, etc)
         */
        private AFont[] all_fonts;

        /*
         * switch the following around to get the set of TTF that works for
         * your document
         */
  // when using Arial      private static final String TTF_REGULAR = "Arial.ttf";
        private static final int TTF_REGULAR_SIZE = 10;
        private static final String TTF_REGULAR = "Roboto-Regular.ttf";
        //        private static final String TTF_REGULAR = "Roboto-Condensed.ttf";
 // when using Arial       private static final String TTF_BOLD = "Arial Bold.ttf";
        private static final int TTF_BOLD_SIZE = 10;
        private static final String TTF_BOLD = "Roboto-Bold.ttf";
  //      private static final String TTF_BOLD = "Roboto-BoldCondensed.ttf";
//        private static final String TTF_BOLD = "Roboto-Black.ttf";
 // when using Arial       private static final String TTF_CAPTION  = "Arial Italic.ttf";
        private static final int TTF_CAPTION_SIZE = 8;
        private static final String TTF_CAPTION  = "Roboto-Italic.ttf";
  //      private static final String TTF_CAPTION  = "Roboto-CondensedItalic.ttf";
// when using Arial        private static final String TTF_DETAILS = "Arial.ttf";
        private static final int TTF_DETAILS_SIZE = 8;
        private static final String TTF_DETAILS = "Roboto-Regular.ttf";
  //      private static final String TTF_DETAILS = "Roboto-Condensed.ttf";
   // when using Arial     private static final String TTF_ITALIC = "Arial Italic.ttf";
        private static final String TTF_ITALIC = "Roboto-Italic.ttf";
        private static final int TTF_ITALIC_SIZE = 10;
        
        
       /* private float text_font_height;
        private float text_font_bold_height;
        private float caption_font_height;
        private float details_font_height;
        private float printer_symbol_font_height;
        */
        private static MyFontFactory the_instance = null;
        
        /*
         * no constructor
         */
        
        public static MyFontFactory getInstance(String the_type,
                                          PDDocument the_doc) throws Exception
        {
            if (the_instance == null)
            {
                if (the_type.equals("TTF"))
                {
                    // ok
                }
                else
                {
                    if (the_type.equals("TYPE1"))
                    {
                        // ok
                    }
                    else
                    {
                        throw new Exception("Font Factory MUST be invoked with either 'TTF' or 'TYPE1'");
                    }
                }
                the_instance = new MyFontFactory(the_type,the_doc); // private constructor
            }
            return the_instance;
        }
        
        private MyFontFactory(String the_type,PDDocument the_doc) throws Exception
        {
            all_fonts = new AFont[total_fonts];
            /*
             * the type has already been checked, so go ahead without error checking
             */
            if (the_type.equals("TTF"))
            {
                this.font_type = TTF; // set it for TrueType
                /*
                 * FONTS loaded ONCE only. The other methods will NOT work
                 * unless this is performed now
                 */
                this.all_fonts[FONT_TEXT] =  new AFont(PDTrueTypeFont.loadTTF(the_doc,TTF_REGULAR),TTF_REGULAR_SIZE);
                this.all_fonts[FONT_TEXT_BOLD] =  new AFont(PDTrueTypeFont.loadTTF(the_doc,TTF_BOLD),TTF_BOLD_SIZE);
                this.all_fonts[FONT_CAPTION] =  new AFont(PDTrueTypeFont.loadTTF(the_doc,TTF_CAPTION),TTF_CAPTION_SIZE);
                this.all_fonts[FONT_DETAILS] =  new AFont(PDTrueTypeFont.loadTTF(the_doc,TTF_DETAILS),TTF_DETAILS_SIZE);
                this.all_fonts[FONT_ITALIC] =  new AFont(PDTrueTypeFont.loadTTF(the_doc,TTF_ITALIC),TTF_ITALIC_SIZE);
                /*
                 * following currently ONLY used for head type 5
                 */
                this.all_fonts[FONT_PRINTER_SYMBOLS] =  new AFont(PDTrueTypeFont.loadTTF(the_doc,"prinoorg.ttf"),
                                                                  12);
            }
            if (the_type.equals("TYPE1"))
            {
                this.font_type = TYPE1;  // set it for Type 1 (see list above)
                this.all_fonts[FONT_TEXT] = new AFont(PDType1Font.HELVETICA,10); 
                this.all_fonts[FONT_TEXT_BOLD] =  new AFont(PDType1Font.HELVETICA_BOLD,10);
                this.all_fonts[FONT_CAPTION] =  new AFont(PDType1Font.HELVETICA_OBLIQUE,8); // image caption
                this.all_fonts[FONT_DETAILS] = new AFont(PDType1Font.TIMES_ROMAN,8); // detail lines smaller
//                this.all_fonts[FONT_DETAILS] = new AFont(PDType1Font.HELVETICA,8); // detail lines smaller
                this.all_fonts[FONT_ITALIC] = new AFont(PDType1Font.HELVETICA_OBLIQUE,10); // normal size, just italic
                /*
                 * following currently ONLY used for head type 5
                 */
                this.all_fonts[FONT_PRINTER_SYMBOLS] = new AFont(PDType1Font.ZAPF_DINGBATS,12);
            }
            
            /*
             * set up heading fonts, using the information from above
             */
            this.all_heads = new AFont[6];
            
            this.all_heads[0] = this.all_fonts[FONT_TEXT_BOLD];
            this.all_heads[1] = this.all_fonts[FONT_TEXT_BOLD];
            this.all_heads[2] = this.all_fonts[FONT_TEXT_BOLD];
            this.all_heads[3] = this.all_fonts[FONT_TEXT_BOLD];
            this.all_heads[4] = new AFont(PDType1Font.SYMBOL,12);   // ALWAYS special: 4
            this.all_heads[5] = this.all_fonts[FONT_PRINTER_SYMBOLS]; // only used in heads FOR NOW
            
        } // end constructor
    
        public  PDSimpleFont getAFont(int position) 
        {
            return all_fonts[position].the_font;
        }
    
        /*
         * nothing specified, used default text
         */
        public  PDSimpleFont getAFont() 
        {
            return all_fonts[FONT_TEXT].the_font;
        }
        
        public int getAFontSize(int position)
        {
            return all_fonts[position].the_size;
        }
        
        public int getAFontSize()
        {
            return all_fonts[FONT_TEXT].the_size;
        }
        
        public float getAFontHeight(int position)
        {
            return all_fonts[position].the_height;
        }
        
        public float getAFontHeight()
        {
            return all_fonts[FONT_TEXT].the_height;
        }
        
        /*
         * eliminates a LOT of duplicated code
         */
        public float getTextWidth(String s) throws Exception
        {
            return (all_fonts[FONT_TEXT].the_font.getStringWidth(s)/1000) * 
            (float) (all_fonts[FONT_TEXT].the_size);
            
        }
        
        /*
         * eliminates a LOT of duplicated code
         */
        public float getTextWidth(int position, String s) throws Exception
        {
            return (all_fonts[position].the_font.getStringWidth(s)/1000) * 
            (float) (all_fonts[position].the_size);
            
        }
        /*
         * eliminates a LOT of duplicated code
         */
        public float getTextHeadWidth(int position, String s) throws Exception
        {
            return (all_heads[position].the_font.getStringWidth(s)/1000) * 
            (float) (all_heads[position].the_size);
            
        }
        /*
         * eliminates a LOT of duplicated code
         */
        public void setPDFStreamFont(int position, PDPageContentStream s) throws Exception
        {
            s.setFont(
            all_fonts[position].the_font,
            all_fonts[position].the_size);
        }
        /*
         * eliminates a LOT of duplicated code
         */
        public void setPDFStreamHeadFont(int position, PDPageContentStream s) throws Exception
        {
            s.setFont(
                all_heads[position].the_font,
                all_heads[position].the_size);
        }

        /*
         * default font for normal text (not specified)
         */
        public void setPDFStreamFont(PDPageContentStream s) throws Exception
        {
                setPDFStreamFont(FONT_TEXT,s);
        }
        
        
        public PDSimpleFont getHeadFont(int head_number)
        {
            return all_heads[head_number].the_font; // set up during constructor
        }
        
        public int getHeadFontSize(int head_number)
        {
            return all_heads[head_number].the_size; // set up during constructor
        }
        
        public float getHeadFontHeight(int head_number)
        {
            return all_heads[head_number].the_height; // set up during constructor
        }

        /*
         * The "bullet" item character is different for different font
         * types. The mapping indicates that the using program
         * will have the FONT_PRINTER_SYMBOLS active. Use
         * the appropriate character for the TrueType vs Type1.
         */
        public String getBulletCharacters()
        {
            if (this.font_type == TTF)
            {
                // TrueType special char
                return "  " + (char)149 + " "; // two spaces, special char (149,0225,) , one space
//                return "  " + (char)183 + " "; // two spaces, special char (183, ), one space
            }
            else
            {
                // Type1 special char ZapfDingbats black circle
//                return "  " + (char)0x06c + " "; // two spaces, special char, one space
  // standard font, middle dot char (too tiny)
//                return "  " + (char)0x0b7 + " "; // two spaces, special char, one space
             // standard font, bullet (wrong?)
//                return "  " + (char)0x080 + " "; // two spaces, special char, one space
  // standard font, section marker
                return "  " + (char)0x0a7 + " "; // two spaces, special char, one space
            }
        }

        /*
         * The "separator" item will be a single line
         * with printer-style symbols in it. The TTF fonts
         * have some, but so do the ZapfDingbats.
         */
        public String getSeparatorCharacters()
        {
            if (this.font_type == TTF)
            {
                // TrueType special char
                return "n n n n n n"; // the "printer ornaments" TTF has these
//                return "L L L L L L"; // the "printer ornaments" TTF has these
            }
            else
            {
                // Type1 special char ZapfDingbats
                return "a a a a a a";
            }
        }
        
        /*
         * Container for a font, includes not only the font, but
         * its height (calculated from font metrics), and the desired font
         * size (such as 10 point, 12 point, etc)
         */
        public class AFont
        {
            public PDSimpleFont the_font;
            public float the_height;
            public int the_size;
            
            public AFont(PDSimpleFont f) throws Exception
            {
                this(f,12); // default 12 point
            }
            /*
             * Constructor with only font and type size
             */
            public AFont(PDSimpleFont f, 
                         int s) throws Exception
            {
                this.the_font = f;
                this.the_size = s;
                float h = f.getFontDescriptor().getFontBoundingBox().getHeight()/1000;

                /*
                 * the font_type and test value are  global to the containing object
                 */
                if (font_type == TTF)
                {
                    this.the_height = h * .9f // a little smaller by 10%
                    * (float) s;
                /*
                 * WORKAROUND for the problem of wrong /widths
                 * when using truetype fonts. We simply limit
                 * the number of possible character codes to 255.
                 *
                 * For normal English characters this should work.
FIXED in 1.7.1, so this code commented out
                 List awid = this.the_font.getWidths();
                    if (awid != null)
                    {
                        if (awid.size() > 255)
                        {
                            List<Float> widths = new ArrayList<Float>();
                            
                            for (int charCode = 0; charCode <= 255; charCode++) {
                                widths.add(this.the_font.getFontWidth(charCode));
                            }
                            this.the_font.setWidths(widths);
                            this.the_font.setFirstChar(0);
                            this.the_font.setLastChar(255);
                        } // end if too large
                    }
                    */
                }
                
                else
                {
                    this.the_height = h * 1.05f // a little larger by 5%
                    * (float) s;
                }
            } // end constructor
        } // end AFont
        
    } // end myfontFactory
