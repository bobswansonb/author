import java.io.PrintWriter;
import java.io.File;
import java.util.ArrayList;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.TreeMap;
import java.util.Map;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayDeque;

// for JSON processing

/*
 * Modified 
 * Fri 21 Aug 2020 09:02:37 AM CDT
 *
 * WARNING NOTE, etc, this is the REVEAL formatted HTML creator
 *
 */

/*
 * Adapted from an existing Project-Agnostic, Format-Specific  object
 */


public  class CreateSpecialContentREVEAL extends HTMLContentCreator
{
    /*
     * Special creation for Reveal.js, many notes do not apply!
     *
     */
    private static final String g_file_extensionq = ".html\"";
    private static final String g_file_extension = ".html";
    

    /*
     * project-specific strings that are to be replaced
     *
     * key = string to search for in boilerplate
     * 
     * value = ReplacementString object that has the
     *   replacement string, and a flag to indicate special processing
     */
	/*
	 * key is our name for the string group we want to write out
	 * payload is TextContent object that has the text content
	 */
	public Map g_texts = new TreeMap();


	// instance initializer
	{
		g_texts.put("static_header", new TextContent("static_header")); // boilerplate, not written to any particular file
	}

    TreeMap project_keys = null; 
		JsonProperties options = null;
    
	public Map g_special_keys_map = new TreeMap();
	// instance initializer
	{
		// not used with reveal g_special_keys_map.put("PROJECT_FRONT_MATTER",new SpecialKeyPROJECT_TEXT("::")); // try the double colon delimiter
		// project file is fine g_special_keys_map.put("PROJECT_KEYWORDS", new SpecialKeyPROJECT_DESCRIPTION());
		g_special_keys_map.put("PROJECT_COPYRIGHT",new SpecialKeyPROJECT_COPYRIGHT());
	}
    
    public CreateSpecialContentREVEAL() throws Exception
    {
        /*
         * read in the data that is used by this object
         */
        Map userData = 
		BookUtils.readJSON(this.getClass().getName(),false); // no debug
        
        // userData is a Map containing the named arrays
        
	Iterator inner = g_texts.values().iterator(); // we don't care about the keys right now
	while (inner.hasNext())
	{
		TextContent content = (TextContent)inner.next();
		content.create(userData); // get text content from JSON, based on format (VANLIGHT, e.g.)
	}
	/*
	 * unlike typical HTML, we have a single entry for "boilerplate"
	 * because we don't depend on VANILLA or anything such, all layout is
	 * done by reveal.js
	 */
	boilerplate = BookUtils.getPropertiesFromJSON(userData,"boilerplate",
		this.getClass().getName());
		 /*
		 * following are PROJECT-specific set of keyword/values
		 */
		key_values = BookUtils.getPropertiesFromJSON("project.json",
			"project");
		// debug System.out.println("project-specific key_values: " + key_values);

		/*
		 * REVEAL now has a single option REVEAL_IMAGE_FORMAT
		 *   it is in this Properties object
		 */
		options = BookUtils.getPropertiesFromJSON("options.json",
		"format_options"); 

    } // end instantiation
    
    /*
     * nothing for now
     */
    public void createTitlePage(Object out, Object notused) throws Exception
    {
}
    
    public void createMetadata(Object out) throws Exception
    {
        // NO metadata right now, the object is null
      //  PrintWriter pr = (PrintWriter)out; // this cast HAS TO WORK
        
    } // end create metadata
    
	/*
	 * TOC content will be passed as the second
	 * object
	 */
    public void createStaticHeaders(Object out,
	Object toc_content,
	AuxiliaryInformation aux) throws Exception
    {
	createStaticHeaders(out,toc_content); // no pass through aux
	}
    public void createStaticHeaders(Object out,
	Object toc_content) throws Exception
    {
        PrintWriter pr = (PrintWriter)out; // this cast HAS TO WORK
	TextContent contentx = (TextContent)g_texts.get("static_header");
	/*
	System.out.println("contentx: " + contentx);
	System.out.println("key_values: " + key_values);
	System.out.println("g_special_keys_map: " + g_special_keys_map);
	*/
	BookUtils.createAPageWithModifications(pr,contentx,key_values,g_special_keys_map,false); // NO close
	} // end make static headers
        
        public void renderIndex(PrintWriter pr, List all, int level) throws Exception
        {
// nothing for now
	}
    
        public void renderIndexList(PrintWriter pr, 
                                    List all, 
                                    boolean breadcrumbs,
                                    ArrayDeque to_top,
                                    int index_type) throws Exception
        {
}
                    
        
        public void renderIndexGroupList(PrintWriter pr, 
                                         List all, 
                                         int level,
                                         ArrayDeque to_top,
                                         int index_type) throws Exception
        {
}
    
    
        public void renderBookmarks(PrintWriter pr, List all) throws Exception
        {
        }
        
        public void createTOC(PrintWriter pr, 
                              Map all_maps,
                              List appendixes,
                              Map index_flags) throws Exception
        {
		// too complex, is in the REVEALSink object
        }

   	/*     
   	 * WORK ON THIS     
   	 */     
        public void endPageSeq(
            PrintWriter pr) throws Exception
        {
            pr.print("<p >\n");
// FOLLOWING LINK SEEMS UNNEXESSARY and it doesn't go anywhere
//            pr.print("<a href=\"index" + g_file_extension + "#state_listing\">All Indexes</a></p>\n"); // back to indexes on main web page
            pr.print("<a href=\"index" + g_file_extension + "#top\">All Indexes</a></p>\n"); // back to indexes on main web page
            pr.print("<p >\n");
            pr.print("<a href=\"#top\">Table of Contents</a></p>\n");
            pr.print(gT("LINE_BREAK"));
            pr.print(gT("LINE_BREAK"));
            pr.print(gT("LINE_BREAK"));
            pr.print(gT("LINE_BREAK"));
            pr.print("</p>\n");
            /*
             * HERE HERE finish the page
             */
            pr.print("</div> <!-- skeleton container wrapper -->\n"); 
                        pr.print("</body>\n");
            pr.print("</html>\n");
        } // end endpageseq
    
        public void startFlow(
            PrintWriter pr,
            String page_number_string) throws Exception
        {
        }

	/*
	 * this code is unique to this format-specific object
	 * each switch() position is based on the special_key
	 * position.
	 * 
	 * return a String that will be pushed back into the
	 * JSON structure, as a complete replacement
	 * for the original string in which the special key was
	 * found
	 */
	public String specialReplacementProcessing(ReplacementString rval)
	{
		StringBuffer result = new StringBuffer();
		switch (rval.flag)
		{
			case 0:
			{
			    	// "PROJECT_FRONT_MATTER"
				result.append("<h1>"); // front is a heading
			    	String xx[] = rval.rep.split(":"); // 1 (one) colon delimiter
        			for (int inner  = 0 ; inner < xx.length ; inner++)
        			{
        				result.append(xx[inner] +
        					"<br/>" + "<br/>");
        				if (inner == 0)   // first line only
        				{
        					result.append("</h1>" +
						"<p style=\"text-align: center;\">" +
						"<br/>");

        				} // end first line only
				}  // end loop through all strings to be treated as separate lines         
				result.append("</p>");
			    	break;
			} // end 0 which is PROJECT_FRONT_MATTER
			case 1:
			{
			    	// "PROJECT_KEYWORDS"
			    	String xx[] = rval.rep.split(":"); // 1 (one) colon delimiter
        			for (int inner  = 0 ; inner < xx.length ; inner++)
        			{
        				result.append(xx[inner] +
        					","); // keywords are comma-delimited
				}  // end loop through all strings to be treated as separate lines         
				result.append("web"); // dummy for end
			    	break;
			} // end 1 which is PROJECT_KEYWORDS
			case 2:
			{
			    	// "PROJECT_COPYRIGHT"
				result.append( "<meta name=\"copyright\" content=\"" + 
			    	rval.rep + "\"/>");
			    	break;
			} // end 2 which is PROJECT_COPYRIGHT
		} // end switch on special code segments
		return result.toString();		
	}	// end specialReplacementProcessing
	
	
    public Object getMetaData(String json_file) throws Exception
    {
    		return getProjectSpecificMetaData(json_file); // use helper in base class
    }

    public String getProjectKeyValue(String xx) throws Exception
    {
	    return options.getProperty(xx); // may be null
	}
        
}  // end create special content for reveal.js special HTML 
