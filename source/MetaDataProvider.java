public interface MetaDataProvider
{
    /*
     * Updated 
     * Thu 13 Aug 2020 01:56:08 PM CDT
     *
     * As of 8/13/20, removing any getMetaData references
     *
     * Now that all project-agnostic, format-specific
     * objects are made generic, we need to
     * inject metadata into the boilerplate that
     * is specific to each project at runtime (not during
     * compilation, or embedded in a special format- and project-specific
     * JSON file).
     *
     * Methods:
     *
     * DEFUNCT
     *  getMetaData -- passes key to project-specific
     *     metadata JSON file, returns a pointer to the
     *     JSON objects containing that information.
     *     This is needed to get things started, and the
     *     return value may be of interest to the SINK code
     *     because it may inject project-specific metadata
     *     on its own (example: EPUB content.opf file)
     *    THIS MAY BE REMOVED, as we are no longer performing
     *    in-place modifications.
     *
     * DEFUNCT
     * modifyMetaData -- actually invokes modification
     *    of the existing boilerplate inside the format-specific
     *    object, so that it will be project-specific
     *    THIS HAS BEEN REMOVED, as we are no longer performing
     *    in-place modifications.
     */
    
    //public void modifyMetaData() throws Exception;
    
    //public Object getMetaData(String json_file) throws Exception;
    
    	//public String specialReplacementProcessing(ReplacementString rval);

	/*
	 * allow a CreateSpecialContent (or its helpers) to get a particular 
	 * PROJECT key value. Project-specific information is not
	 * held at the Sink level, as we are project-agnostic these days.
	 * (this remains while other stuff is removed
	 */
	public String getProjectKeyValue(String key) throws Exception;

    
} // end MetaDataProvider interface
