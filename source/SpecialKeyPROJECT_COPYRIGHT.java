import java.util.Iterator;
import java.util.List;

// for JSON processing
// 

import com.bfo.json.Json;


public class SpecialKeyPROJECT_COPYRIGHT implements SpecialKeyProcessor
{
	String []text = {"<meta name='copyright' content='","'/>"}; // this is for HTML

	/*
	 * no param constructor
	 */
	public SpecialKeyPROJECT_COPYRIGHT()
	{
		 String[] copyright_stuff = {"<meta name='copyright' content='","'/>"}; // this is for HTML
		text = copyright_stuff; // for HTML 
	}

	/*
	 * constructor that takes the markup as an array of string (2 really)
	 */
	public SpecialKeyPROJECT_COPYRIGHT(String []t)
	{
		text = t;
	}

	public String replace(String val) throws Exception
	{
		/*
		* "PROJECT_COPYRIGHT"
		*/
		if (text.length != 2)
		{
				throw new java.lang.IllegalArgumentException("Boilerplate length must be 2, have: " + text.length);
		}
		return text[0] + val + text[1];
	} // end replace
} // end special key project copyright text

