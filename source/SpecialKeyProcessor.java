import java.util.Map;
import java.util.TreeMap;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.List;
import java.util.Properties;
import java.util.Date;

import java.text.DateFormat;

/*
 * Updated:
 *
 * Sat 01 Aug 2020 06:13:26 AM CDT
 *
 * Interface for the special key processors
 *
 */
public interface SpecialKeyProcessor
{


	public String replace(String val) throws Exception;


} // end special key processor
