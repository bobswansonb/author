import java.util.Iterator;
import java.util.List;

// for JSON processing
// 

import com.bfo.json.Json;


public class SpecialKeyPROJECT_TEXT implements SpecialKeyProcessor
{
	String delimiter = ":"; // default

	/*
	 * no param constructor
	 */
	public SpecialKeyPROJECT_TEXT()
	{
		this(":"); // default is single colon
	}

	/*
	 * constructor that takes a delimiter string
	 * for the paragraphs
	 */
	public SpecialKeyPROJECT_TEXT(String del)
	{
		delimiter = del;
	}

	public String replace(String val)
	{
		/*
		* "PROJECT_TEXT"
		*
		* multiple paragraphs, split with delimiter
		* (by default a single colon (:))
		*
		* OR, a List as made by toString() in the Json
		* object. Apparently, a simple Object like that
		* can be both read and written by Json. (nice)
		*/
		Json test = null;
		List items = null;
		try
		{
			test = Json.read(val); // can it parse its own output and tell us what it is?
			// got this far without throwing exception, we think its a List, lets see
			items = test.listValue(); // if this barfs, something is wrong inside passed info
			// DONT FORGET, the List contains Json objects, which we hope are wrapping single Strings
			System.out.println("input: " + val + ", Json sez: " + test.type());
		}
		catch (Exception ex)
		{
			// Json didnt like it, we just process it
			if (ex instanceof java.lang.IllegalArgumentException)
			{
				// expected under normal circumstances, we try to ignore (unless debugging)
			}
			else
			{
				System.out.println("input: " + val + ", Json sez error: " + ex); // we are intrigued
			}
		}
		StringBuffer result = new StringBuffer();
		if (test != null)
		{
			// Json figured it out, we appear to have  a List, nothing fancier, please
			Iterator ii = items.iterator(); // dont forget IMMUTABLE
			while (ii.hasNext())
			{
				Json jj = (Json)ii.next(); // each item is a Json that contains a string
				result.append("<p>" + jj.stringValue() + "</p>"); // paragraph
			} // end loop on all strings that were inside the List
			//first paragraph may need break(s) "<br/>");
		}
		else
		{
			// Json didnt like it, we process as delimited strings
			String xx[] = val.split(delimiter); // split on the provided delimiter
			for (int inner  = 0 ; inner < xx.length ; inner++)
			{
				result.append("<p>" + xx[inner] +
				"</p>"); // paragraph
				//first paragraph may need break(s) "<br/>");
			}  // end loop through all strings to be treated as separate lines         
		}
		return result.toString();
	} // end replace
} // end special key project text

