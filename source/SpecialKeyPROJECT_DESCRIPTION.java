
/*
 * special key processor. This is used for the 'description'
 * section of the metadata of a web page. So, it has some rules:
 *
 * 1) no apostrophes! (single quotes), as these are used in the HTML
 * 2) ONE LINE only please. What is passed is appended with some
 *    boilerplate and the entire string passed back for insertion
 *    in the metadata tag
 *
 */

public class SpecialKeyPROJECT_DESCRIPTION implements SpecialKeyProcessor
{
	/*
	 * default, note rules above about single line and no apostrophes
	 */
	String boilerplate = 
		" This page is part of the Swanson web pages, which cover many of our interests, including: cruising, past RV travel, travel in general, photography, ships and boats, collecting, including postal history interests for Bob, our reading and music interests, as well as some links to web sites we often use to find out about the weather, political opinion, and investing.";

	/*
	 * no param constructor
	 */
	public SpecialKeyPROJECT_DESCRIPTION()
	{
		this(" This page is part of the Swanson web pages, which cover many of our interests, including: cruising, past RV travel, travel in general, photography, ships and boats, collecting, including postal history interests for Bob, our reading and music interests, as well as some links to web sites we often use to find out about the weather, political opinion, and investing.");
	}
	/*
	 * constructor where the caller supplies
	 * the 'boilerplate' to be appended to the
	 * descriptions
	 *
	 * Note that the description passed is PROJECT-specific.
	 * The boilerplate is probably personalized for the 
	 * website owner/designer, and is generally not project-specific.
	 */
	public SpecialKeyPROJECT_DESCRIPTION(String boil)
	{
		boilerplate = boil;
	}

	public String replace(String val)
	{
		/*
		 * "PROJECT_DESCRIPTION"
		 *
		 * Use the text, but add some more
		 */
		return val + boilerplate;
	} // end replace
} // end special key project description
